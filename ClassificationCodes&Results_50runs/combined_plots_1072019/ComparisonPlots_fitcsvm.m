%_______________________Preparing Test Data_________________________________
Accuracies_Test11runsAF = csvread('Accuracies_Test11runsAF.csv');
Accuracies_Test11runsGA8 = csvread('Accuracies_Test11runsGA8.csv');
Accuracies_Test11runsGA9 = csvread('Accuracies_Test11runsGA9.csv');
Accuracies_Test11runsGA10 = csvread('Accuracies_Test11runsGA10.csv');
Accuracies_Test11runsGA11 = csvread('Accuracies_Test11runsGA11.csv');
Accuracies_Test11runsPSO8 = csvread('Accuracies_Test11runsPSO8.csv');
Accuracies_Test11runsPSO9 = csvread('Accuracies_Test11runsPSO9.csv');
Accuracies_Test11runsPSO10 = csvread('Accuracies_Test11runsPSO10.csv');
Accuracies_Test11runsPSO11 = csvread('Accuracies_Test11runsPSO11.csv');
Accuracies_Test50runsGWO8 = csvread('Accuracies_Test50runsGWO8.csv');
Accuracies_Test50runsGWO9 = csvread('Accuracies_Test50runsGWO9.csv');
Accuracies_Test50runsGWO10 = csvread('Accuracies_Test50runsGWO10.csv');
Accuracies_Test50runsGWO11 = csvread('Accuracies_Test50runsGWO11.csv');
%_______________________Preparing Train Data_________________________________
Accuracies_Train11runsAF = csvread('Accuracies_Train11runsAF.csv');
Accuracies_Train11runsGA8 = csvread('Accuracies_Train11runsGA8.csv');
Accuracies_Train11runsGA9 = csvread('Accuracies_Train11runsGA9.csv');
Accuracies_Train11runsGA10 = csvread('Accuracies_Train11runsGA10.csv');
Accuracies_Train11runsGA11 = csvread('Accuracies_Train11runsGA11.csv');
Accuracies_Train11runsPSO8 = csvread('Accuracies_Train11runsPSO8.csv');
Accuracies_Train11runsPSO9 = csvread('Accuracies_Train11runsPSO9.csv');
Accuracies_Train11runsPSO10 = csvread('Accuracies_Train11runsPSO10.csv');
Accuracies_Train11runsPSO11 = csvread('Accuracies_Train11runsPSO11.csv');
Accuracies_Train50runsGWO8 = csvread('Accuracies_Train50runsGWO8.csv');
Accuracies_Train50runsGWO9 = csvread('Accuracies_Train50runsGWO9.csv');
Accuracies_Train50runsGWO10 = csvread('Accuracies_Train50runsGWO10.csv');
Accuracies_Train50runsGWO11 = csvread('Accuracies_Train50runsGWO11.csv');

classifierNames = ['KNN','DA_pl','DA_dl','NB_n','NB_k','DT','SVM_l','SVM_r','SVM_p2','RF'];
diffFeatures = {'AF','BGA_8','BGA_9','BGA_10','BGA_11','BPSO_8','BPSO_9','BPSO_10','BPSO_11','BGWO_8','BGWO_9','BGWO_10','BGWO_11'};

%______________________________________________________________________
%____________________Accuracies Training_______________________________
%______________________________________________________________________
%All Accuracies for KNN classifier with Testing Set
AccTestKNN = Accuracies_Test11runsAF(:,1);
AccTestKNN = [AccTestKNN,Accuracies_Test11runsGA8(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsGA9(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsGA10(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsGA11(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsPSO8(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsPSO9(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsPSO10(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsPSO11(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test50runsGWO8(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test50runsGWO9(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test50runsGWO10(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test50runsGWO11(:,1)];

figure()
boxplot(AccTestKNN,diffFeatures);
title('Accuracy with 50 iterations using KNN classifier & different feature sets (Test Data)');
ylabel('Accurcies with KNN');
xlabel('Different Features');
savefig('KNNAccuracytest.fig');

%All Accuracies for DA_pl classifier with Testing Set
AccTestDA_pl = Accuracies_Test11runsAF(:,2);
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test11runsGA8(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test11runsGA9(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test11runsGA10(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test11runsGA11(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test11runsPSO8(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test11runsPSO9(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test11runsPSO10(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test11runsPSO11(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test50runsGWO8(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test50runsGWO9(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test50runsGWO10(:,2)];
AccTestDA_pl = [AccTestDA_pl,Accuracies_Test50runsGWO11(:,2)];

figure()
boxplot(AccTestDA_pl,diffFeatures);
title('Accuracy with 50 iterations using DA_pl classifier & different feature sets (Test Data)');
ylabel('Accurcies with DA_pl');
xlabel('Different Features');
savefig('DA_plAccuracytest.fig');

%All Accuracies for DA_dl classifier with Testing Set
AccTestDA_dl = Accuracies_Test11runsAF(:,3);
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test11runsGA8(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test11runsGA9(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test11runsGA10(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test11runsGA11(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test11runsPSO8(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test11runsPSO9(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test11runsPSO10(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test11runsPSO11(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test50runsGWO8(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test50runsGWO9(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test50runsGWO10(:,3)];
AccTestDA_dl = [AccTestDA_dl,Accuracies_Test50runsGWO11(:,3)];

figure()
boxplot(AccTestDA_dl,diffFeatures);
title('Accuracy with 50 iterations using DA_dl classifier & different feature sets (Test Data)');
ylabel('Accurcies with DA_dl');
xlabel('Different Features');
savefig('DA_dlAccuracytest.fig');

%All Accuracies for NB_n classifier with Testing Set
AccTestNB_n = Accuracies_Test11runsAF(:,4);
AccTestNB_n = [AccTestNB_n,Accuracies_Test11runsGA8(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test11runsGA9(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test11runsGA10(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test11runsGA11(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test11runsPSO8(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test11runsPSO9(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test11runsPSO10(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test11runsPSO11(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test50runsGWO8(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test50runsGWO9(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test50runsGWO10(:,4)];
AccTestNB_n = [AccTestNB_n,Accuracies_Test50runsGWO11(:,4)];

figure()
boxplot(AccTestNB_n,diffFeatures);
title('Accuracy with 50 iterations using NB_n classifier & different feature sets (Test Data)');
ylabel('Accurcies with NB_n');
xlabel('Different Features');
savefig('NB_nAccuracytest.fig');

%All Accuracies for NB_k classifier with Testing Set
AccTestNB_k = Accuracies_Test11runsAF(:,5);
AccTestNB_k = [AccTestNB_k,Accuracies_Test11runsGA8(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test11runsGA9(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test11runsGA10(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test11runsGA11(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test11runsPSO8(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test11runsPSO9(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test11runsPSO10(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test11runsPSO11(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test50runsGWO8(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test50runsGWO9(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test50runsGWO10(:,5)];
AccTestNB_k = [AccTestNB_k,Accuracies_Test50runsGWO11(:,5)];

figure()
boxplot(AccTestNB_k,diffFeatures);
title('Accuracy with 50 iterations using NB_k classifier & different feature sets (Test Data)');
ylabel('Accurcies with NB_k');
xlabel('Different Features');
savefig('NB_kAccuracytest.fig');

%All Accuracies for DT classifier with Testing Set
AccTestDT = Accuracies_Test11runsAF(:,6);
AccTestDT = [AccTestDT,Accuracies_Test11runsGA8(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsGA9(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsGA10(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsGA11(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsPSO8(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsPSO9(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsPSO10(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsPSO11(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test50runsGWO8(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test50runsGWO9(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test50runsGWO10(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test50runsGWO11(:,6)];

figure()
boxplot(AccTestDT,diffFeatures);
title('Accuracy with 50 iterations using DT classifier & different feature sets (Test Data)');
ylabel('Accurcies with DT');
xlabel('Different Features');
savefig('DTAccuracytest.fig');

%All Accuracies for SVM_l classifier with Testing Set
AccTestSVM_l = Accuracies_Test11runsAF(:,7);
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test11runsGA8(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test11runsGA9(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test11runsGA10(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test11runsGA11(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test11runsPSO8(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test11runsPSO9(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test11runsPSO10(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test11runsPSO11(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test50runsGWO8(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test50runsGWO9(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test50runsGWO10(:,7)];
AccTestSVM_l = [AccTestSVM_l,Accuracies_Test50runsGWO11(:,7)];

figure()
boxplot(AccTestSVM_l,diffFeatures);
title('Accuracy with 50 iterations using SVM_l classifier & different feature sets (Test Data)');
ylabel('Accurcies with SVM_l');
xlabel('Different Features');
savefig('SVM_lAccuracytest.fig');

%All Accuracies for SVM_r classifier with Testing Set
AccTestSVM_r = Accuracies_Test11runsAF(:,8);
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test11runsGA8(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test11runsGA9(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test11runsGA10(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test11runsGA11(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test11runsPSO8(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test11runsPSO9(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test11runsPSO10(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test11runsPSO11(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test50runsGWO8(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test50runsGWO9(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test50runsGWO10(:,8)];
AccTestSVM_r = [AccTestSVM_r,Accuracies_Test50runsGWO11(:,8)];

figure()
boxplot(AccTestSVM_r,diffFeatures);
title('Accuracy with 50 iterations using SVM_r classifier & different feature sets (Test Data)');
ylabel('Accurcies with SVM_r');
xlabel('Different Features');
savefig('SVM_rAccuracytest.fig');

%All Accuracies for SVM_p2 classifier with Testing Set
AccTestSVM_p2 = Accuracies_Test11runsAF(:,9);
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test11runsGA8(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test11runsGA9(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test11runsGA10(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test11runsGA11(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test11runsPSO8(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test11runsPSO9(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test11runsPSO10(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test11runsPSO11(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test50runsGWO8(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test50runsGWO9(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test50runsGWO10(:,9)];
AccTestSVM_p2 = [AccTestSVM_p2,Accuracies_Test50runsGWO11(:,9)];

figure()
boxplot(AccTestSVM_p2,diffFeatures);
title('Accuracy with 50 iterations using SVM_p2 classifier & different feature sets (Test Data)');
ylabel('Accurcies with SVM_p2');
xlabel('Different Features');
savefig('SVM_p2Accuracytest.fig');

%All Accuracies for RF classifier with Testing Set
AccTestRF = Accuracies_Test11runsAF(:,10);
AccTestRF = [AccTestRF,Accuracies_Test11runsGA8(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsGA9(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsGA10(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsGA11(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsPSO8(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsPSO9(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsPSO10(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsPSO11(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test50runsGWO8(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test50runsGWO9(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test50runsGWO10(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test50runsGWO11(:,10)];

figure()
boxplot(AccTestRF,diffFeatures);
title('Accuracy with 50 iterations using RF classifier & different feature sets (Test Data)');
ylabel('Accurcies with RF');
xlabel('Different Features');
savefig('RFAccuracytest.fig');

%______________________________________________________________________
%____________________Accuracies Training_______________________________
%______________________________________________________________________

%All Accuracies for KNN classifier with Training Set
AccTrainKNN = Accuracies_Train11runsAF(:,1);
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsGA8(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsGA9(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsGA10(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsGA11(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsPSO8(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsPSO9(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsPSO10(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsPSO11(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train50runsGWO8(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train50runsGWO9(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train50runsGWO10(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train50runsGWO11(:,1)];

figure()
boxplot(AccTrainKNN,diffFeatures);
title('Accuracy with 50 iterations using KNN classifier & different feature sets (Train Data)');
ylabel('Accurcies with KNN');
xlabel('Different Features');
savefig('KNNAccuracytrain.fig');

%All Accuracies for DA_pl classifier with Training Set
AccTrainDA_pl = Accuracies_Train11runsAF(:,2);
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train11runsGA8(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train11runsGA9(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train11runsGA10(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train11runsGA11(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train11runsPSO8(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train11runsPSO9(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train11runsPSO10(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train11runsPSO11(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train50runsGWO8(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train50runsGWO9(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train50runsGWO10(:,2)];
AccTrainDA_pl = [AccTrainDA_pl,Accuracies_Train50runsGWO11(:,2)];

figure()
boxplot(AccTrainDA_pl,diffFeatures);
title('Accuracy with 50 iterations using DA_pl classifier & different feature sets (Train Data)');
ylabel('Accurcies with DA_pl');
xlabel('Different Features');
savefig('DA_plAccuracytrain.fig');

%All Accuracies for DA_dl classifier with Training Set
AccTrainDA_dl = Accuracies_Train11runsAF(:,3);
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train11runsGA8(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train11runsGA9(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train11runsGA10(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train11runsGA11(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train11runsPSO8(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train11runsPSO9(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train11runsPSO10(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train11runsPSO11(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train50runsGWO8(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train50runsGWO9(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train50runsGWO10(:,3)];
AccTrainDA_dl = [AccTrainDA_dl,Accuracies_Train50runsGWO11(:,3)];

figure()
boxplot(AccTrainDA_dl,diffFeatures);
title('Accuracy with 50 iterations using DA_dl classifier & different feature sets (Train Data)');
ylabel('Accurcies with DA_dl');
xlabel('Different Features');
savefig('DA_dlAccuracytrain.fig');

%All Accuracies for NB_n classifier with Training Set
AccTrainNB_n = Accuracies_Train11runsAF(:,4);
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train11runsGA8(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train11runsGA9(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train11runsGA10(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train11runsGA11(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train11runsPSO8(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train11runsPSO9(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train11runsPSO10(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train11runsPSO11(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train50runsGWO8(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train50runsGWO9(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train50runsGWO10(:,4)];
AccTrainNB_n = [AccTrainNB_n,Accuracies_Train50runsGWO11(:,4)];

figure()
boxplot(AccTrainNB_n,diffFeatures);
title('Accuracy with 50 iterations using NB_n classifier & different feature sets (Train Data)');
ylabel('Accurcies with NB_n');
xlabel('Different Features');
savefig('NB_nAccuracytrain.fig');

%All Accuracies for NB_k classifier with Training Set
AccTrainNB_k = Accuracies_Train11runsAF(:,5);
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train11runsGA8(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train11runsGA9(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train11runsGA10(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train11runsGA11(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train11runsPSO8(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train11runsPSO9(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train11runsPSO10(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train11runsPSO11(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train50runsGWO8(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train50runsGWO9(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train50runsGWO10(:,5)];
AccTrainNB_k = [AccTrainNB_k,Accuracies_Train50runsGWO11(:,5)];

figure()
boxplot(AccTrainNB_k,diffFeatures);
title('Accuracy with 50 iterations using NB_k classifier & different feature sets (Train Data)');
ylabel('Accurcies with NB_k');
xlabel('Different Features');
savefig('NB_kAccuracytrain.fig');

%All Accuracies for DT classifier with Training Set
AccTrainDT = Accuracies_Train11runsAF(:,6);
AccTrainDT = [AccTrainDT,Accuracies_Train11runsGA8(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsGA9(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsGA10(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsGA11(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsPSO8(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsPSO9(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsPSO10(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsPSO11(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train50runsGWO8(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train50runsGWO9(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train50runsGWO10(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train50runsGWO11(:,6)];

figure()
boxplot(AccTrainDT,diffFeatures);
title('Accuracy with 50 iterations using DT classifier & different feature sets (Train Data)');
ylabel('Accurcies with DT');
xlabel('Different Features');
savefig('DTAccuracytrain.fig');

%All Accuracies for SVM_l classifier with Training Set
AccTrainSVM_l = Accuracies_Train11runsAF(:,7);
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train11runsGA8(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train11runsGA9(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train11runsGA10(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train11runsGA11(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train11runsPSO8(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train11runsPSO9(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train11runsPSO10(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train11runsPSO11(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train50runsGWO8(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train50runsGWO9(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train50runsGWO10(:,7)];
AccTrainSVM_l = [AccTrainSVM_l,Accuracies_Train50runsGWO11(:,7)];

figure()
boxplot(AccTrainSVM_l,diffFeatures);
title('Accuracy with 50 iterations using SVM_l classifier & different feature sets (Train Data)');
ylabel('Accurcies with SVM_l');
xlabel('Different Features');
savefig('SVM_lAccuracytrain.fig');

%All Accuracies for SVM_r classifier with Training Set
AccTrainSVM_r = Accuracies_Train11runsAF(:,8);
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train11runsGA8(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train11runsGA9(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train11runsGA10(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train11runsGA11(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train11runsPSO8(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train11runsPSO9(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train11runsPSO10(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train11runsPSO11(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train50runsGWO8(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train50runsGWO9(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train50runsGWO10(:,8)];
AccTrainSVM_r = [AccTrainSVM_r,Accuracies_Train50runsGWO11(:,8)];

figure()
boxplot(AccTrainSVM_r,diffFeatures);
title('Accuracy with 50 iterations using SVM_r classifier & different feature sets (Train Data)');
ylabel('Accurcies with SVM_r');
xlabel('Different Features');
savefig('SVM_rAccuracytrain.fig');

%All Accuracies for SVM_p2 classifier with Training Set
AccTrainSVM_p2 = Accuracies_Train11runsAF(:,9);
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train11runsGA8(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train11runsGA9(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train11runsGA10(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train11runsGA11(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train11runsPSO8(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train11runsPSO9(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train11runsPSO10(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train11runsPSO11(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train50runsGWO8(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train50runsGWO9(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train50runsGWO10(:,9)];
AccTrainSVM_p2 = [AccTrainSVM_p2,Accuracies_Train50runsGWO11(:,9)];

figure()
boxplot(AccTrainSVM_p2,diffFeatures);
title('Accuracy with 50 iterations using SVM_p2 classifier & different feature sets (Train Data)');
ylabel('Accurcies with SVM_p2');
xlabel('Different Features');
savefig('SVM_p2Accuracytrain.fig');

%All Accuracies for RF classifier with Training Set
AccTrainRF = Accuracies_Train11runsAF(:,10);
AccTrainRF = [AccTrainRF,Accuracies_Train11runsGA8(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsGA9(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsGA10(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsGA11(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsPSO8(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsPSO9(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsPSO10(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsPSO11(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train50runsGWO8(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train50runsGWO9(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train50runsGWO10(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train50runsGWO11(:,10)];

figure()
boxplot(AccTrainRF,diffFeatures);
title('Accuracy with 50 iterations using RF classifier & different feature sets (Train Data)');
ylabel('Accurcies with RF');
xlabel('Different Features');
savefig('RFAccuracytrain.fig');
