%-------------------------------------------------------------------------%
%  Machine learning algorithms source codes demo version                  %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jKNN(trainlabel,trainfeat,testlabel,testfeat,k,kfold)
rng('default'); 
% Perform k-nearest neighbor classifier with Euclidean distance
ModelKNN=fitcknn(trainfeat,trainlabel,'NumNeighbors',k,'Distance','euclidean');
%Predicting the testing and training labels
PredTest = predict(ModelKNN,testfeat);
PredTrain = predict(ModelKNN,trainfeat);
% Confusion matrix
confmatTest = confusionmat(testlabel,PredTest); 
confmatTrain = confusionmat(trainlabel,PredTrain);
% disp("*********KNN*********")

% disp("Confusion Matrix Test set")
% disp(confmatTest)
%Calculate evaluation metrics on Test set
[AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest] = evalmetrics(confmatTest);

% disp("Confusion Matrix Train set")
% disp(confmatTrain)
%Calculate evaluation metrics on Train set
[AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = evalmetrics(confmatTrain);

%plot confusion matrix for both testing and training sets
% figure(1)
% nametest = 'KNN-Testing-set-';
% plotconfusion(testlabel',PredTest',nametest);
% figure(2)
% nametrain = 'KNN-Train-set-';
% plotconfusion(trainlabel',PredTrain',nametrain);
end


