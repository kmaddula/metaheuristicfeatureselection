%-------------------------------------------------------------------------%
%  Machine learning algorithms source codes demo version                  %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jNB(trainlabel,trainfeat,testlabel,testfeat,Dist,kfold,i)
switch Dist
	case'n'; Dist='normal';
	case'k'; Dist='kernel';
end
rng('default'); 
% fprintf('------Distribution: %s \n',Dist)
% Perform Naive Bayes
ModelNB=fitcnb(trainfeat,trainlabel,'Distribution',Dist);
 
PredTest = predict(ModelNB,testfeat);
PredTrain = predict(ModelNB,trainfeat);
% Confusion matrix
confmatTest = confusionmat(testlabel,PredTest); 
confmatTrain = confusionmat(trainlabel,PredTrain);

% disp("Confusion Matrix Test set")
% disp(confmatTest)
%Calculate evaluation metrics on Test set
[AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest] = evalmetrics(confmatTest);

% disp("Confusion Matrix Train set")
% disp(confmatTrain)
%Calculate evaluation metrics on Train set
[AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = evalmetrics(confmatTrain);

%plot confusion matrix for both testing and training sets
% figure(i*90)
% nametest = ['NB-Testing-set-',Dist,'-'];
% plotconfusion(testlabel',PredTest',nametest);
% figure(i*91)
% nametrain = ['NB-Training-set-',Dist,'-'];
% plotconfusion(trainlabel',PredTrain',nametrain); 
end

