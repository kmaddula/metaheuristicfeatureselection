%-----------------------------------GA Features------------------------
%---------------------------------Data with top 8 features from GA
clear; close all; clc;
%load global indexes
load trainingIndex7030.mat
load testingIndex7030.mat 
for j = 1:50
% disp('Binary GA Version**********');
%load dataset
disp(j)
featMatrixGWO =csvread('fmtop9BGWO.csv');
%For 70 - 30
% load trainidx7030.mat
% load testidx7030.mat
%For 75-25
%  load trainidx7525.mat
%  load testidx7525.mat
%For 80-20
% load trainidx8020.mat
% load testidx8020.mat

%Slicing the data
traindataGWO = featMatrixGWO(trainingIndex(:,j),:);
testdataGWO = featMatrixGWO(testingIndex(:,j),:);
%training Data
trainlabelGWO = traindataGWO(:,size(traindataGWO,2));
trainfeatGWO = traindataGWO(:,1:(size(traindataGWO,2)-1));
%test Data
testlabelGWO = testdataGWO(:,size(testdataGWO,2));
testfeatGWO = testdataGWO(:,1:(size(testdataGWO,2)-1));
%-----------------------------------Applying Classifiers with top 8 features-----------
% (1) Perform k-nearest neighbor (KNN)
    disp('KNN')
    kfold=10; k=3; % k-value in KNN
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jKNN(trainlabelGWO,trainfeatGWO,testlabelGWO,testfeatGWO,k,kfold); 
    %Save KNN evaluation metrics into vectors
    %testing metrics
    Accuracy_Test_KNNGWO(j) = AccuracyTest;
    Recall_Test_KNNGWO(j) = RecallTest;
    Specificity_Test_KNNGWO(j) = SpecificityTest;
    PPV_Test_KNNGWO(j) = PPVTest;
    NPV_Test_KNNGWO(j) = NPVTest;
    FMeasure_Test_KNNGWO(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_KNNGWO(j) = AccuracyTrain;
    Recall_Train_KNNGWO(j) = RecallTrain;
    Specificity_Train_KNNGWO(j) = SpecificityTrain;
    PPV_Train_KNNGWO(j) = PPVTrain;
    NPV_Train_KNNGWO(j) = NPVTrain;
    FMeasure_Train_KNNGWO(j) = FMeasureTrain;
    
    % (2) Perform discriminate analysis (DA)
%     disp("*********DA*********")
    kfold=10;
    % The Discriminate can selected as follows:
    % 'l' : linear 
    % 'q' : quadratic
    % 'pq': pseudoquadratic
    % 'pl': pseudolinear
    % 'dl': diaglinear
    % 'dq': diagquadratic
    Disc = {'l','q','pq','pl','dl','dq'};
    for i= 4:5
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDA(trainlabelGWO,trainfeatGWO,testlabelGWO,testfeatGWO,Disc{i},kfold,i);
        if(i==4)
            disp('DA-psuedolinear')
            %testing metrics
            Accuracy_Test_DAdlGWO(j) = AccuracyTest;
            Recall_Test_DAdlGWO(j) = RecallTest;
            Specificity_Test_DAdlGWO(j) = SpecificityTest;
            PPV_Test_DAdlGWO(j) = PPVTest;
            NPV_Test_DAdlGWO(j) = NPVTest;
            FMeasure_Test_DAdlGWO(j) = FMeasureTest;
            
            %training metrics
            Accuracy_Train_DAdlGWO(j) = AccuracyTrain;
            Recall_Train_DAdlGWO(j) = RecallTrain;
            Specificity_Train_DAdlGWO(j) = SpecificityTrain;
            PPV_Train_DAdlGWO(j) = PPVTrain;
            NPV_Train_DAdlGWO(j) = NPVTrain;
            FMeasure_Train_DAdlGWO(j) = FMeasureTrain;
            
        end
        if(i==5)
            disp('DA-diaglinear')
            %testing metrics
            Accuracy_Test_DAdqGWO(j) = AccuracyTest;
            Recall_Test_DAdqGWO(j) = RecallTest;
            Specificity_Test_DAdqGWO(j) = SpecificityTest;
            PPV_Test_DAdqGWO(j) = PPVTest;
            NPV_Test_DAdqGWO(j) = NPVTest;
            FMeasure_Test_DAdqGWO(j) = FMeasureTest;
            
            %training metrics
            Accuracy_Train_DAdqGWO(j) = AccuracyTrain;
            Recall_Train_DAdqGWO(j) = RecallTrain;
            Specificity_Train_DAdqGWO(j) = SpecificityTrain;
            PPV_Train_DAdqGWO(j) = PPVTrain;
            NPV_Train_DAdqGWO(j) = NPVTrain;
            FMeasure_Train_DAdqGWO(j) = FMeasureTrain;
        end
    end

    % (3) Perform Naive Bayes (NB)
%     disp("*********NB*********")
    kfold=10; Dist=['n','k']; 
    % The Distribution can selected as follows:
    % 'n' : normal distribution 
    % 'k' : kernel distribution
    for i= 1:2
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jNB(trainlabelGWO,trainfeatGWO,testlabelGWO,testfeatGWO,Dist(i),kfold,i); 
        if(i==1)
            disp('NB-normal')
            %testing metrics
            Accuracy_Test_NBnGWO(j) = AccuracyTest;
            Recall_Test_NBnGWO(j) = RecallTest;
            Specificity_Test_NBnGWO(j) = SpecificityTest;
            PPV_Test_NBnGWO(j) = PPVTest;
            NPV_Test_NBnGWO(j) = NPVTest;
            FMeasure_Test_NBnGWO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_NBnGWO(j) = AccuracyTrain;
            Recall_Train_NBnGWO(j) = RecallTrain;
            Specificity_Train_NBnGWO(j) = SpecificityTrain;
            PPV_Train_NBnGWO(j) = PPVTrain;
            NPV_Train_NBnGWO(j) = NPVTrain;
            FMeasure_Train_NBnGWO(j) = FMeasureTrain;
        end
        if(i==2)
            disp('NB-kernel')
            %testing metrics
            Accuracy_Test_NBkGWO(j) = AccuracyTest;
            Recall_Test_NBkGWO(j) = RecallTest;
            Specificity_Test_NBkGWO(j) = SpecificityTest;
            PPV_Test_NBkGWO(j) = PPVTest;
            NPV_Test_NBkGWO(j) = NPVTest;
            FMeasure_Test_NBkGWO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_NBkGWO(j) = AccuracyTrain;
            Recall_Train_NBkGWO(j) = RecallTrain;
            Specificity_Train_NBkGWO(j) = SpecificityTrain;
            PPV_Train_NBkGWO(j) = PPVTrain;
            NPV_Train_NBkGWO(j) = NPVTrain;
            FMeasure_Train_NBkGWO(j) = FMeasureTrain;
        end
    end

    % (4) Perform decision tree (DT)
    disp('DT')
    kfold=10; nSplit=50; % Number of split in DT
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDT(trainlabelGWO,trainfeatGWO,testlabelGWO,testfeatGWO,nSplit,kfold);
    %testing metrics
    Accuracy_Test_DTGWO(j) = AccuracyTest;
    Recall_Test_DTGWO(j) = RecallTest;
    Specificity_Test_DTGWO(j) = SpecificityTest;
    PPV_Test_DTGWO(j) = PPVTest;
    NPV_Test_DTGWO(j) = NPVTest;
    FMeasure_Test_DTGWO(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_DTGWO(j) = AccuracyTrain;
    Recall_Train_DTGWO(j) = RecallTrain;
    Specificity_Train_DTGWO(j) = SpecificityTrain;
    PPV_Train_DTGWO(j) = PPVTrain;
    NPV_Train_DTGWO(j) = NPVTrain;
    FMeasure_Train_DTGWO(j) = FMeasureTrain;

    % (5) Perform support vector machine
    kernel = ['l','r','p'];
%     disp("*********SVM*********")
    for i= 1:3
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jSVM(trainlabelGWO,trainfeatGWO,testlabelGWO,testfeatGWO,kernel(i),i);
        if(i==1)
            disp('SVM-linear')
            %testing metrics
            Accuracy_Test_SVMlGWO(j) = AccuracyTest;
            Recall_Test_SVMlGWO(j) = RecallTest;
            Specificity_Test_SVMlGWO(j) = SpecificityTest;
            PPV_Test_SVMlGWO(j) = PPVTest;
            NPV_Test_SVMlGWO(j) = NPVTest;
            FMeasure_Test_SVMlGWO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMlGWO(j) = AccuracyTrain;
            Recall_Train_SVMlGWO(j) = RecallTrain;
            Specificity_Train_SVMlGWO(j) = SpecificityTrain;
            PPV_Train_SVMlGWO(j) = PPVTrain;
            NPV_Train_SVMlGWO(j) = NPVTrain;
            FMeasure_Train_SVMlGWO(j) = FMeasureTrain;
        end
        if(i==2)
            disp('SVM-Gaussian Radial Basis function')
            %testing metrics
            Accuracy_Test_SVMrGWO(j) = AccuracyTest;
            Recall_Test_SVMrGWO(j) = RecallTest;
            Specificity_Test_SVMrGWO(j) = SpecificityTest;
            PPV_Test_SVMrGWO(j) = PPVTest;
            NPV_Test_SVMrGWO(j) = NPVTest;
            FMeasure_Test_SVMrGWO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMrGWO(j) = AccuracyTrain;
            Recall_Train_SVMrGWO(j) = RecallTrain;
            Specificity_Train_SVMrGWO(j) = SpecificityTrain;
            PPV_Train_SVMrGWO(j) = PPVTrain;
            NPV_Train_SVMrGWO(j) = NPVTrain;
            FMeasure_Train_SVMrGWO(j) = FMeasureTrain;
        end
        if(i==3)
            disp('SVM-Polinomial-Degree2')
            %testing metrics
            Accuracy_Test_SVMpGWO(j) = AccuracyTest;
            Recall_Test_SVMpGWO(j) = RecallTest;
            Specificity_Test_SVMpGWO(j) = SpecificityTest;
            PPV_Test_SVMpGWO(j) = PPVTest;
            NPV_Test_SVMpGWO(j) = NPVTest;
            FMeasure_Test_SVMpGWO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMpGWO(j) = AccuracyTrain;
            Recall_Train_SVMpGWO(j) = RecallTrain;
            Specificity_Train_SVMpGWO(j) = SpecificityTrain;
            PPV_Train_SVMpGWO(j) = PPVTrain;
            NPV_Train_SVMpGWO(j) = NPVTrain;
            FMeasure_Train_SVMpGWO(j) = FMeasureTrain;
        end
    end
    % 'r' : radial basis function  
    % 'l' : linear function 
    % 'p' : polynomial function 
    % 'g' : gaussian function

    % (6) Perform random forest (RF)
    disp('RF')
    kfold=10; nBag=50; % Number of bags in RF
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jRF(trainlabelGWO,trainfeatGWO,testlabelGWO,testfeatGWO,nBag,kfold);
    %testing metrics
    Accuracy_Test_RFGWO(j) = AccuracyTest;
    Recall_Test_RFGWO(j) = RecallTest;
    Specificity_Test_RFGWO(j) = SpecificityTest;
    PPV_Test_RFGWO(j) = PPVTest;
    NPV_Test_RFGWO(j) = NPVTest;
    FMeasure_Test_RFGWO(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_RFGWO(j) = AccuracyTrain;
    Recall_Train_RFGWO(j) = RecallTrain;
    Specificity_Train_RFGWO(j) = SpecificityTrain;
    PPV_Train_RFGWO(j) = PPVTrain;
    NPV_Train_RFGWO(j) = NPVTrain;
    FMeasure_Train_RFGWO(j) = FMeasureTrain;
end
Accuracies_TestGWO = [Accuracy_Test_KNNGWO;Accuracy_Test_DAdlGWO;Accuracy_Test_DAdqGWO;Accuracy_Test_NBnGWO;Accuracy_Test_NBkGWO;Accuracy_Test_DTGWO;Accuracy_Test_SVMlGWO;Accuracy_Test_SVMrGWO;Accuracy_Test_SVMpGWO;Accuracy_Test_RFGWO];
Accuracies_TestGWO = Accuracies_TestGWO';
Accuracies_Test_meanGWO = mean(Accuracies_TestGWO);
Accuracies_TrainGWO = [Accuracy_Train_KNNGWO;Accuracy_Train_DAdlGWO;Accuracy_Train_DAdqGWO;Accuracy_Train_NBnGWO;Accuracy_Train_NBkGWO;Accuracy_Train_DTGWO;Accuracy_Train_SVMlGWO;Accuracy_Train_SVMrGWO;Accuracy_Train_SVMpGWO;Accuracy_Train_RFGWO];
Accuracies_TrainGWO = Accuracies_TrainGWO';
Accuracies_Train_meanGWO = mean(Accuracies_TrainGWO);
Recalls_TestGWO = [Recall_Test_KNNGWO;Recall_Test_DAdlGWO;Recall_Test_DAdqGWO;Recall_Test_NBnGWO;Recall_Test_NBkGWO; Recall_Test_DTGWO;Recall_Test_SVMlGWO;Recall_Test_SVMrGWO;Recall_Test_SVMpGWO;Recall_Test_RFGWO];
Recalls_TestGWO = Recalls_TestGWO';
Recalls_Test_meanGWO = mean(Recalls_TestGWO);
Recalls_TrainGWO = [Recall_Train_KNNGWO;Recall_Train_DAdlGWO;Recall_Train_DAdqGWO;Recall_Train_NBnGWO;Recall_Train_NBkGWO;Recall_Train_DTGWO; Recall_Train_SVMlGWO;Recall_Train_SVMrGWO;Recall_Train_SVMpGWO;Recall_Train_RFGWO];
Recalls_TrainGWO = Recalls_TrainGWO';
Recalls_Train_meanGWO = mean(Recalls_TrainGWO);
Specificities_TestGWO = [Specificity_Test_KNNGWO;Specificity_Test_DAdlGWO;Specificity_Test_DAdqGWO;Specificity_Test_NBnGWO;Specificity_Test_NBkGWO;Specificity_Test_DTGWO;Specificity_Test_SVMlGWO;Specificity_Test_SVMrGWO;Specificity_Test_SVMpGWO;Specificity_Test_RFGWO];
Specificities_TestGWO = Specificities_TestGWO';
Specificities_Test_meanGWO = mean(Specificities_TestGWO);
Specificities_TrainGWO = [Specificity_Train_KNNGWO;Specificity_Train_DAdlGWO;Specificity_Train_DAdqGWO;Specificity_Train_NBnGWO;Specificity_Train_NBkGWO;Specificity_Train_DTGWO;Specificity_Train_SVMlGWO;Specificity_Train_SVMrGWO;Specificity_Train_SVMpGWO;Specificity_Train_RFGWO];
Specificities_TrainGWO = Specificities_TrainGWO';
Specificities_Train_meanGWO = mean(Specificities_TrainGWO);
NTCPrecisions_TestGWO = [PPV_Test_KNNGWO;PPV_Test_DAdlGWO;PPV_Test_DAdqGWO;PPV_Test_NBnGWO;PPV_Test_NBkGWO;PPV_Test_DTGWO;PPV_Test_SVMlGWO;PPV_Test_SVMrGWO;PPV_Test_SVMpGWO;PPV_Test_RFGWO];
NTCPrecisions_TestGWO = NTCPrecisions_TestGWO';
NTCPrecisions_Test_meanGWO = mean(NTCPrecisions_TestGWO);
NTCPrecisions_TrainGWO = [PPV_Train_KNNGWO;PPV_Train_DAdlGWO;PPV_Train_DAdqGWO;PPV_Train_NBnGWO;PPV_Train_NBkGWO;PPV_Train_DTGWO;PPV_Train_SVMlGWO;PPV_Train_SVMrGWO;PPV_Train_SVMpGWO;PPV_Train_RFGWO];
NTCPrecisions_TrainGWO = NTCPrecisions_TrainGWO';
NTCPrecisions_Train_meanGWO = mean(NTCPrecisions_TrainGWO);
TCPrecisions_TestGWO = [NPV_Test_KNNGWO;NPV_Test_DAdlGWO;NPV_Test_DAdqGWO;NPV_Test_NBnGWO;NPV_Test_NBkGWO;NPV_Test_DTGWO;NPV_Test_SVMlGWO;NPV_Test_SVMrGWO;NPV_Test_SVMpGWO;NPV_Test_RFGWO];
TCPrecisions_TestGWO = TCPrecisions_TestGWO';
TCPrecisions_Test_meanGWO = mean(TCPrecisions_TestGWO);
TCPrecisions_TrainGWO = [NPV_Train_KNNGWO;NPV_Train_DAdlGWO;NPV_Train_DAdqGWO;NPV_Train_NBnGWO;NPV_Train_NBkGWO;NPV_Train_DTGWO;NPV_Train_SVMlGWO;NPV_Train_SVMrGWO;NPV_Train_SVMpGWO;NPV_Train_RFGWO];
TCPrecisions_TrainGWO = TCPrecisions_TrainGWO';
TCPrecisions_Train_meanGWO = mean(TCPrecisions_TrainGWO);
FMeasures_TestGWO = [FMeasure_Test_KNNGWO;FMeasure_Test_DAdlGWO;FMeasure_Test_DAdqGWO;FMeasure_Test_NBnGWO;FMeasure_Test_NBkGWO;FMeasure_Test_DTGWO;FMeasure_Test_SVMlGWO;FMeasure_Test_SVMrGWO;FMeasure_Test_SVMpGWO;FMeasure_Test_RFGWO];
FMeasures_TestGWO = FMeasures_TestGWO';
FMeasures_Test_meanGWO = mean(FMeasures_TestGWO);
FMeasures_TrainGWO = [FMeasure_Train_KNNGWO;FMeasure_Train_DAdlGWO;FMeasure_Train_DAdqGWO;FMeasure_Train_NBnGWO;FMeasure_Train_NBkGWO;FMeasure_Train_DTGWO;FMeasure_Train_SVMlGWO;FMeasure_Train_SVMrGWO;FMeasure_Train_SVMpGWO;FMeasure_Train_RFGWO];
FMeasures_TrainGWO = FMeasures_TrainGWO';
FMeasures_Train_meanGWO = mean(FMeasures_TrainGWO);
MeanMetrics_TestGWO = [Accuracies_Test_meanGWO;Recalls_Test_meanGWO;Specificities_Test_meanGWO;NTCPrecisions_Test_meanGWO;TCPrecisions_Test_meanGWO;FMeasures_Test_meanGWO];
MeanMetrics_TrainGWO = [Accuracies_Train_meanGWO;Recalls_Train_meanGWO;Specificities_Train_meanGWO;NTCPrecisions_Train_meanGWO;TCPrecisions_Train_meanGWO;FMeasures_Train_meanGWO];
%save BGA results
csvwrite('Accuracies_Test50runsGWO.csv',Accuracies_TestGWO);
csvwrite('Accuracies_Train50runsGWO.csv',Accuracies_TrainGWO);
csvwrite('Recalls_Test50runsGWO.csv',Recalls_TestGWO);
csvwrite('Recalls_Train50runsGWO.csv',Recalls_TrainGWO);
csvwrite('Specificities_Test50runsGWO.csv',Specificities_TestGWO);
csvwrite('Specificities_Train50runsGWO.csv',Specificities_TrainGWO);
csvwrite('NTCPrecisions_Test50runsGWO.csv',NTCPrecisions_TestGWO);
csvwrite('NTCPrecisions_Train50runsGWO.csv',NTCPrecisions_TrainGWO);
csvwrite('TCPrecisions_Test50runsGWO.csv',TCPrecisions_TestGWO);
csvwrite('TCPrecisions_Train50runsGWO.csv',TCPrecisions_TrainGWO);
csvwrite('FMeasures_Test50runsGWO.csv',FMeasures_TestGWO);
csvwrite('FMeasures_Train50runsGWO.csv',FMeasures_TrainGWO);
csvwrite('MeanMetrics_Test50runsGWO.csv',MeanMetrics_TestGWO);
csvwrite('MeanMetrics_Train50runsGWO.csv',MeanMetrics_TrainGWO);

%plot figures for Test data set
classifierNames = categorical({'KNN','DA-pl','DA-dl','NB-n','NB-k','DT','SVM-l','SVM-r','SVM-p2','RF'});
%Accuracies barh graphs
figure(1)
barh(classifierNames,Accuracies_Test_meanGWO)
title('Accuracy using BGWO Top-9 features');
xlabel('Accuracy');
saveas(gcf,'top9BGWO50runsAccuracy','jpeg');

%Recalls barh graphs
figure(2)
barh(classifierNames,Recalls_Test_meanGWO)
title('Recall using BGWO Top-9 features');
xlabel('Recall');
saveas(gcf,'top9BGWO50runsRecall','jpeg');

%Specificities barh graphs
figure(3)
barh(classifierNames,Specificities_Test_meanGWO)
title('Specificity using BGWO Top-9 features');
xlabel('Specificity');
saveas(gcf,'top9BGWO50runsSpecificity','jpeg');

%NTCPrecisions barh graphs
figure(4)
barh(classifierNames,NTCPrecisions_Test_meanGWO)
title('Non Tumor Class Precision using BGWO Top-9 features');
xlabel('Non Tumor Class Precision');
saveas(gcf,'top9BGWO50runsNTCP','jpeg');

%TCPrecisions barh graphs
figure(5)
barh(classifierNames,TCPrecisions_Test_meanGWO)
title('Tumor Class Precision using BGWO Top-9 features');
xlabel('Tumor Class Precision');
saveas(gcf,'top9BGWO50runsTCP','jpeg');

%FMeasures barh graphs
figure(6)
barh(classifierNames,FMeasures_Test_meanGWO)
title('FMeasure using BGWO Top-9 features');
xlabel('FMeasure');
saveas(gcf,'top9BGWO50runsFMeasure','jpeg');

%BoxPlot for all the evaluation metrics for all the classifiers
%Accuracy- TesTest
figure(7)
xlabel('Classifier');
ylabel('Accuracy');
title('Classification Accuracy Using Top-9 features from BGWO');
boxplot(Accuracies_TestGWO,classifierNames);
saveas(gcf,'box_top9BGWO50runsAccuracy','jpeg');

%Recall- TesTest
figure(8)
xlabel('Classifier');
ylabel('Recall');
title('Recall Using Top-9 features from BGWO');
boxplot(Recalls_TestGWO,classifierNames);
saveas(gcf,'box_top9BGWO50runsRecall','jpeg');

%Specificities- test
figure(9)
xlabel('Classifier');
ylabel('Specificity');
title('Specificity Using Top-9 features from BGWO');
boxplot(Specificities_TestGWO,classifierNames);
saveas(gcf,'box_top9BGWO50runsSpecificities','jpeg');

%Non Tumor Class Precision- test
figure(10)
xlabel('Classifier');
ylabel('Non Tumor Class Precision');
title('Non Tumor Class Precision Using Top-9 features from BGWO');
boxplot(NTCPrecisions_TestGWO,classifierNames);
saveas(gcf,'box_top9BGWO50runsNTCP','jpeg');

%Tumor Class Precision- test
figure(11)
xlabel('Classifier');
ylabel('Tumor Class Precision');
title('Tumor Class Precision Using Top-9 features from BGWO');
boxplot(TCPrecisions_TestGWO,classifierNames);
saveas(gcf,'box_top9BGWO50runsTCP','jpeg');

%Fmeasure- test
figure(12)
xlabel('Classifier');
ylabel('FMeasure');
title('FMeasure Using Top-9 features from BGWO');
boxplot(FMeasures_TestGWO,classifierNames);
saveas(gcf,'box_top9BGWO50runsFmeasure','jpeg');