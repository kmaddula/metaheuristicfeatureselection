%-----------------------------------GA Features------------------------
%---------------------------------Data with top 8 features from GA
clear; close all; clc;
%load global indexes
load trainingIndex7030.mat
load testingIndex7030.mat 
for j = 1:50
% disp('Binary GA Version**********');
%load dataset
disp(j)
featMatrixGA = csvread('fmtop9BGA.csv');
%For 70 - 30
% load trainidx7030.mat
% load testidx7030.mat
%For 75-25
%  load trainidx7525.mat
%  load testidx7525.mat
%For 80-20
% load trainidx8020.mat
% load testidx8020.mat

%Slicing the data
traindataGA = featMatrixGA(trainingIndex(:,j),:);
testdataGA = featMatrixGA(testingIndex(:,j),:);
%training Data
trainlabelGA = traindataGA(:,size(traindataGA,2));
trainfeatGA = traindataGA(:,1:(size(traindataGA,2)-1));
%test Data
testlabelGA = testdataGA(:,size(testdataGA,2));
testfeatGA = testdataGA(:,1:(size(testdataGA,2)-1));
%-----------------------------------Applying Classifiers with top 8 features-----------
% (1) Perform k-nearest neighbor (KNN)
    disp('KNN')
    kfold=10; k=3; % k-value in KNN
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jKNN(trainlabelGA,trainfeatGA,testlabelGA,testfeatGA,k,kfold); 
    %Save KNN evaluation metrics into vectors
    %testing metrics
    Accuracy_Test_KNNGA(j) = AccuracyTest;
    Recall_Test_KNNGA(j) = RecallTest;
    Specificity_Test_KNNGA(j) = SpecificityTest;
    PPV_Test_KNNGA(j) = PPVTest;
    NPV_Test_KNNGA(j) = NPVTest;
    FMeasure_Test_KNNGA(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_KNNGA(j) = AccuracyTrain;
    Recall_Train_KNNGA(j) = RecallTrain;
    Specificity_Train_KNNGA(j) = SpecificityTrain;
    PPV_Train_KNNGA(j) = PPVTrain;
    NPV_Train_KNNGA(j) = NPVTrain;
    FMeasure_Train_KNNGA(j) = FMeasureTrain;
    
    % (2) Perform discriminate analysis (DA)
%     disp("*********DA*********")
    kfold=10;
    % The Discriminate can selected as follows:
    % 'l' : linear 
    % 'q' : quadratic
    % 'pq': pseudoquadratic
    % 'pl': pseudolinear
    % 'dl': diaglinear
    % 'dq': diagquadratic
    Disc = {'l','q','pq','pl','dl','dq'};
    for i= 4:5
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDA(trainlabelGA,trainfeatGA,testlabelGA,testfeatGA,Disc{i},kfold,i);
        if(i==4)
            disp('DA-psuedolinear')
            %testing metrics
            Accuracy_Test_DAdlGA(j) = AccuracyTest;
            Recall_Test_DAdlGA(j) = RecallTest;
            Specificity_Test_DAdlGA(j) = SpecificityTest;
            PPV_Test_DAdlGA(j) = PPVTest;
            NPV_Test_DAdlGA(j) = NPVTest;
            FMeasure_Test_DAdlGA(j) = FMeasureTest;
            
            %training metrics
            Accuracy_Train_DAdlGA(j) = AccuracyTrain;
            Recall_Train_DAdlGA(j) = RecallTrain;
            Specificity_Train_DAdlGA(j) = SpecificityTrain;
            PPV_Train_DAdlGA(j) = PPVTrain;
            NPV_Train_DAdlGA(j) = NPVTrain;
            FMeasure_Train_DAdlGA(j) = FMeasureTrain;
            
        end
        if(i==5)
            disp('DA-diaglinear')
            %testing metrics
            Accuracy_Test_DAdqGA(j) = AccuracyTest;
            Recall_Test_DAdqGA(j) = RecallTest;
            Specificity_Test_DAdqGA(j) = SpecificityTest;
            PPV_Test_DAdqGA(j) = PPVTest;
            NPV_Test_DAdqGA(j) = NPVTest;
            FMeasure_Test_DAdqGA(j) = FMeasureTest;
            
            %training metrics
            Accuracy_Train_DAdqGA(j) = AccuracyTrain;
            Recall_Train_DAdqGA(j) = RecallTrain;
            Specificity_Train_DAdqGA(j) = SpecificityTrain;
            PPV_Train_DAdqGA(j) = PPVTrain;
            NPV_Train_DAdqGA(j) = NPVTrain;
            FMeasure_Train_DAdqGA(j) = FMeasureTrain;
        end
    end

    % (3) Perform Naive Bayes (NB)
%     disp("*********NB*********")
    kfold=10; Dist=['n','k']; 
    % The Distribution can selected as follows:
    % 'n' : normal distribution 
    % 'k' : kernel distribution
    for i= 1:2
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jNB(trainlabelGA,trainfeatGA,testlabelGA,testfeatGA,Dist(i),kfold,i); 
        if(i==1)
            disp('NB-normal')
            %testing metrics
            Accuracy_Test_NBnGA(j) = AccuracyTest;
            Recall_Test_NBnGA(j) = RecallTest;
            Specificity_Test_NBnGA(j) = SpecificityTest;
            PPV_Test_NBnGA(j) = PPVTest;
            NPV_Test_NBnGA(j) = NPVTest;
            FMeasure_Test_NBnGA(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_NBnGA(j) = AccuracyTrain;
            Recall_Train_NBnGA(j) = RecallTrain;
            Specificity_Train_NBnGA(j) = SpecificityTrain;
            PPV_Train_NBnGA(j) = PPVTrain;
            NPV_Train_NBnGA(j) = NPVTrain;
            FMeasure_Train_NBnGA(j) = FMeasureTrain;
        end
        if(i==2)
            disp('NB-kernel')
            %testing metrics
            Accuracy_Test_NBkGA(j) = AccuracyTest;
            Recall_Test_NBkGA(j) = RecallTest;
            Specificity_Test_NBkGA(j) = SpecificityTest;
            PPV_Test_NBkGA(j) = PPVTest;
            NPV_Test_NBkGA(j) = NPVTest;
            FMeasure_Test_NBkGA(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_NBkGA(j) = AccuracyTrain;
            Recall_Train_NBkGA(j) = RecallTrain;
            Specificity_Train_NBkGA(j) = SpecificityTrain;
            PPV_Train_NBkGA(j) = PPVTrain;
            NPV_Train_NBkGA(j) = NPVTrain;
            FMeasure_Train_NBkGA(j) = FMeasureTrain;
        end
    end

    % (4) Perform decision tree (DT)
    disp('DT')
    kfold=10; nSplit=50; % Number of split in DT
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDT(trainlabelGA,trainfeatGA,testlabelGA,testfeatGA,nSplit,kfold);
    %testing metrics
    Accuracy_Test_DTGA(j) = AccuracyTest;
    Recall_Test_DTGA(j) = RecallTest;
    Specificity_Test_DTGA(j) = SpecificityTest;
    PPV_Test_DTGA(j) = PPVTest;
    NPV_Test_DTGA(j) = NPVTest;
    FMeasure_Test_DTGA(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_DTGA(j) = AccuracyTrain;
    Recall_Train_DTGA(j) = RecallTrain;
    Specificity_Train_DTGA(j) = SpecificityTrain;
    PPV_Train_DTGA(j) = PPVTrain;
    NPV_Train_DTGA(j) = NPVTrain;
    FMeasure_Train_DTGA(j) = FMeasureTrain;

    % (5) Perform support vector machine
    kernel = ['l','r','p'];
%     disp("*********SVM*********")
    for i= 1:3
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jSVM(trainlabelGA,trainfeatGA,testlabelGA,testfeatGA,kernel(i),i);
        if(i==1)
            disp('SVM-linear')
            %testing metrics
            Accuracy_Test_SVMlGA(j) = AccuracyTest;
            Recall_Test_SVMlGA(j) = RecallTest;
            Specificity_Test_SVMlGA(j) = SpecificityTest;
            PPV_Test_SVMlGA(j) = PPVTest;
            NPV_Test_SVMlGA(j) = NPVTest;
            FMeasure_Test_SVMlGA(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMlGA(j) = AccuracyTrain;
            Recall_Train_SVMlGA(j) = RecallTrain;
            Specificity_Train_SVMlGA(j) = SpecificityTrain;
            PPV_Train_SVMlGA(j) = PPVTrain;
            NPV_Train_SVMlGA(j) = NPVTrain;
            FMeasure_Train_SVMlGA(j) = FMeasureTrain;
        end
        if(i==2)
            disp('SVM-Gaussian Radial Basis function')
            %testing metrics
            Accuracy_Test_SVMrGA(j) = AccuracyTest;
            Recall_Test_SVMrGA(j) = RecallTest;
            Specificity_Test_SVMrGA(j) = SpecificityTest;
            PPV_Test_SVMrGA(j) = PPVTest;
            NPV_Test_SVMrGA(j) = NPVTest;
            FMeasure_Test_SVMrGA(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMrGA(j) = AccuracyTrain;
            Recall_Train_SVMrGA(j) = RecallTrain;
            Specificity_Train_SVMrGA(j) = SpecificityTrain;
            PPV_Train_SVMrGA(j) = PPVTrain;
            NPV_Train_SVMrGA(j) = NPVTrain;
            FMeasure_Train_SVMrGA(j) = FMeasureTrain;
        end
        if(i==3)
            disp('SVM-Polinomial-Degree2')
            %testing metrics
            Accuracy_Test_SVMpGA(j) = AccuracyTest;
            Recall_Test_SVMpGA(j) = RecallTest;
            Specificity_Test_SVMpGA(j) = SpecificityTest;
            PPV_Test_SVMpGA(j) = PPVTest;
            NPV_Test_SVMpGA(j) = NPVTest;
            FMeasure_Test_SVMpGA(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMpGA(j) = AccuracyTrain;
            Recall_Train_SVMpGA(j) = RecallTrain;
            Specificity_Train_SVMpGA(j) = SpecificityTrain;
            PPV_Train_SVMpGA(j) = PPVTrain;
            NPV_Train_SVMpGA(j) = NPVTrain;
            FMeasure_Train_SVMpGA(j) = FMeasureTrain;
        end
    end
    % 'r' : radial basis function  
    % 'l' : linear function 
    % 'p' : polynomial function 
    % 'g' : gaussian function

    % (6) Perform random forest (RF)
    disp('RF')
    kfold=10; nBag=50; % Number of bags in RF
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jRF(trainlabelGA,trainfeatGA,testlabelGA,testfeatGA,nBag,kfold);
    %testing metrics
    Accuracy_Test_RFGA(j) = AccuracyTest;
    Recall_Test_RFGA(j) = RecallTest;
    Specificity_Test_RFGA(j) = SpecificityTest;
    PPV_Test_RFGA(j) = PPVTest;
    NPV_Test_RFGA(j) = NPVTest;
    FMeasure_Test_RFGA(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_RFGA(j) = AccuracyTrain;
    Recall_Train_RFGA(j) = RecallTrain;
    Specificity_Train_RFGA(j) = SpecificityTrain;
    PPV_Train_RFGA(j) = PPVTrain;
    NPV_Train_RFGA(j) = NPVTrain;
    FMeasure_Train_RFGA(j) = FMeasureTrain;
end
Accuracies_TestGA = [Accuracy_Test_KNNGA;Accuracy_Test_DAdlGA;Accuracy_Test_DAdqGA;Accuracy_Test_NBnGA;Accuracy_Test_NBkGA;Accuracy_Test_DTGA;Accuracy_Test_SVMlGA;Accuracy_Test_SVMrGA;Accuracy_Test_SVMpGA;Accuracy_Test_RFGA];
Accuracies_TestGA = Accuracies_TestGA';
Accuracies_Test_meanGA = mean(Accuracies_TestGA);
Accuracies_TrainGA = [Accuracy_Train_KNNGA;Accuracy_Train_DAdlGA;Accuracy_Train_DAdqGA;Accuracy_Train_NBnGA;Accuracy_Train_NBkGA;Accuracy_Train_DTGA;Accuracy_Train_SVMlGA;Accuracy_Train_SVMrGA;Accuracy_Train_SVMpGA;Accuracy_Train_RFGA];
Accuracies_TrainGA = Accuracies_TrainGA';
Accuracies_Train_meanGA = mean(Accuracies_TrainGA);
Recalls_TestGA = [Recall_Test_KNNGA;Recall_Test_DAdlGA;Recall_Test_DAdqGA;Recall_Test_NBnGA;Recall_Test_NBkGA; Recall_Test_DTGA;Recall_Test_SVMlGA;Recall_Test_SVMrGA;Recall_Test_SVMpGA;Recall_Test_RFGA];
Recalls_TestGA = Recalls_TestGA';
Recalls_Test_meanGA = mean(Recalls_TestGA);
Recalls_TrainGA = [Recall_Train_KNNGA;Recall_Train_DAdlGA;Recall_Train_DAdqGA;Recall_Train_NBnGA;Recall_Train_NBkGA;Recall_Train_DTGA; Recall_Train_SVMlGA;Recall_Train_SVMrGA;Recall_Train_SVMpGA;Recall_Train_RFGA];
Recalls_TrainGA = Recalls_TrainGA';
Recalls_Train_meanGA = mean(Recalls_TrainGA);
Specificities_TestGA = [Specificity_Test_KNNGA;Specificity_Test_DAdlGA;Specificity_Test_DAdqGA;Specificity_Test_NBnGA;Specificity_Test_NBkGA;Specificity_Test_DTGA;Specificity_Test_SVMlGA;Specificity_Test_SVMrGA;Specificity_Test_SVMpGA;Specificity_Test_RFGA];
Specificities_TestGA = Specificities_TestGA';
Specificities_Test_meanGA = mean(Specificities_TestGA);
Specificities_TrainGA = [Specificity_Train_KNNGA;Specificity_Train_DAdlGA;Specificity_Train_DAdqGA;Specificity_Train_NBnGA;Specificity_Train_NBkGA;Specificity_Train_DTGA;Specificity_Train_SVMlGA;Specificity_Train_SVMrGA;Specificity_Train_SVMpGA;Specificity_Train_RFGA];
Specificities_TrainGA = Specificities_TrainGA';
Specificities_Train_meanGA = mean(Specificities_TrainGA);
NTCPrecisions_TestGA = [PPV_Test_KNNGA;PPV_Test_DAdlGA;PPV_Test_DAdqGA;PPV_Test_NBnGA;PPV_Test_NBkGA;PPV_Test_DTGA;PPV_Test_SVMlGA;PPV_Test_SVMrGA;PPV_Test_SVMpGA;PPV_Test_RFGA];
NTCPrecisions_TestGA = NTCPrecisions_TestGA';
NTCPrecisions_Test_meanGA = mean(NTCPrecisions_TestGA);
NTCPrecisions_TrainGA = [PPV_Train_KNNGA;PPV_Train_DAdlGA;PPV_Train_DAdqGA;PPV_Train_NBnGA;PPV_Train_NBkGA;PPV_Train_DTGA;PPV_Train_SVMlGA;PPV_Train_SVMrGA;PPV_Train_SVMpGA;PPV_Train_RFGA];
NTCPrecisions_TrainGA = NTCPrecisions_TrainGA';
NTCPrecisions_Train_meanGA = mean(NTCPrecisions_TrainGA);
TCPrecisions_TestGA = [NPV_Test_KNNGA;NPV_Test_DAdlGA;NPV_Test_DAdqGA;NPV_Test_NBnGA;NPV_Test_NBkGA;NPV_Test_DTGA;NPV_Test_SVMlGA;NPV_Test_SVMrGA;NPV_Test_SVMpGA;NPV_Test_RFGA];
TCPrecisions_TestGA = TCPrecisions_TestGA';
TCPrecisions_Test_meanGA = mean(TCPrecisions_TestGA);
TCPrecisions_TrainGA = [NPV_Train_KNNGA;NPV_Train_DAdlGA;NPV_Train_DAdqGA;NPV_Train_NBnGA;NPV_Train_NBkGA;NPV_Train_DTGA;NPV_Train_SVMlGA;NPV_Train_SVMrGA;NPV_Train_SVMpGA;NPV_Train_RFGA];
TCPrecisions_TrainGA = TCPrecisions_TrainGA';
TCPrecisions_Train_meanGA = mean(TCPrecisions_TrainGA);
FMeasures_TestGA = [FMeasure_Test_KNNGA;FMeasure_Test_DAdlGA;FMeasure_Test_DAdqGA;FMeasure_Test_NBnGA;FMeasure_Test_NBkGA;FMeasure_Test_DTGA;FMeasure_Test_SVMlGA;FMeasure_Test_SVMrGA;FMeasure_Test_SVMpGA;FMeasure_Test_RFGA];
FMeasures_TestGA = FMeasures_TestGA';
FMeasures_Test_meanGA = mean(FMeasures_TestGA);
FMeasures_TrainGA = [FMeasure_Train_KNNGA;FMeasure_Train_DAdlGA;FMeasure_Train_DAdqGA;FMeasure_Train_NBnGA;FMeasure_Train_NBkGA;FMeasure_Train_DTGA;FMeasure_Train_SVMlGA;FMeasure_Train_SVMrGA;FMeasure_Train_SVMpGA;FMeasure_Train_RFGA];
FMeasures_TrainGA = FMeasures_TrainGA';
FMeasures_Train_meanGA = mean(FMeasures_TrainGA);
MeanMetrics_TestGA = [Accuracies_Test_meanGA;Recalls_Test_meanGA;Specificities_Test_meanGA;NTCPrecisions_Test_meanGA;TCPrecisions_Test_meanGA;FMeasures_Test_meanGA];
MeanMetrics_TrainGA = [Accuracies_Train_meanGA;Recalls_Train_meanGA;Specificities_Train_meanGA;NTCPrecisions_Train_meanGA;TCPrecisions_Train_meanGA;FMeasures_Train_meanGA];
%save BGA results
csvwrite('Accuracies_Test11runsGA.csv',Accuracies_TestGA);
csvwrite('Accuracies_Train11runsGA.csv',Accuracies_TrainGA);
csvwrite('Recalls_Test11runsGA.csv',Recalls_TestGA);
csvwrite('Recalls_Train11runsGA.csv',Recalls_TrainGA);
csvwrite('Specificities_Test11runsGA.csv',Specificities_TestGA);
csvwrite('Specificities_Train11runsGA.csv',Specificities_TrainGA);
csvwrite('NTCPrecisions_Test11runsGA.csv',NTCPrecisions_TestGA);
csvwrite('NTCPrecisions_Train11runsGA.csv',NTCPrecisions_TrainGA);
csvwrite('TCPrecisions_Test11runsGA.csv',TCPrecisions_TestGA);
csvwrite('TCPrecisions_Train11runsGA.csv',TCPrecisions_TrainGA);
csvwrite('FMeasures_Test11runsGA.csv',FMeasures_TestGA);
csvwrite('FMeasures_Train11runsGA.csv',FMeasures_TrainGA);
csvwrite('MeanMetrics_Test11runsGA.csv',MeanMetrics_TestGA);
csvwrite('MeanMetrics_Train11runsGA.csv',MeanMetrics_TrainGA);

%plot figures for Test data set
classifierNames = categorical({'KNN','DA-pl','DA-dl','NB-n','NB-k','DT','SVM-l','SVM-r','SVM-p2','RF'});
%Accuracies barh graphs
figure(1)
barh(classifierNames,Accuracies_Test_meanGA)
title('Accuracy using BGA Top-9 features');
xlabel('Accuracy');
saveas(gcf,'top9BGA50runsAccuracy','jpeg');

%Recalls barh graphs
figure(2)
barh(classifierNames,Recalls_Test_meanGA)
title('Recall using BGA Top-9 features');
xlabel('Recall');
saveas(gcf,'top9BGA50runsRecall','jpeg');

%Specificities barh graphs
figure(3)
barh(classifierNames,Specificities_Test_meanGA)
title('Specificity using BGA Top-9 features');
xlabel('Specificity');
saveas(gcf,'top9BGA50runsSpecificity','jpeg');

%NTCPrecisions barh graphs
figure(4)
barh(classifierNames,NTCPrecisions_Test_meanGA)
title('Non Tumor Class Precision using BGA Top-9 features');
xlabel('Non Tumor Class Precision');
saveas(gcf,'top9BGA50runsNTCP','jpeg');

%TCPrecisions barh graphs
figure(5)
barh(classifierNames,TCPrecisions_Test_meanGA)
title('Tumor Class Precision using BGA Top-9 features');
xlabel('Tumor Class Precision');
saveas(gcf,'top9BGA50runsTCP','jpeg');

%FMeasures barh graphs
figure(6)
barh(classifierNames,FMeasures_Test_meanGA)
title('FMeasure using BGA Top-9 features');
xlabel('FMeasure');
saveas(gcf,'top9BGA50runsFMeasure','jpeg');

%BoxPlot for all the evaluation metrics for all the classifiers
%Accuracy- Testset
figure(7)
xlabel('Classifier');
ylabel('Accuracy');
title('Classification Accuracy Using Top-9 features from BGA');
boxplot(Accuracies_TestGA,classifierNames);
saveas(gcf,'box_top9BGA50runsAccuracy','jpeg');

%Recall- Testset
figure(8)
xlabel('Classifier');
ylabel('Recall');
title('Recall Using Top-9 features from BGA');
boxplot(Recalls_TestGA,classifierNames);
saveas(gcf,'box_top9BGA50runsRecall','jpeg');

%Specificities- test
figure(9)
xlabel('Classifier');
ylabel('Specificity');
title('Specificity Using Top-9 features from BGA');
boxplot(Specificities_TestGA,classifierNames);
saveas(gcf,'box_top9BGA50runsSpecificities','jpeg');

%Non Tumor Class Precision- test
figure(10)
xlabel('Classifier');
ylabel('Non Tumor Class Precision');
title('Non Tumor Class Precision Using Top-9 features from BGA');
boxplot(NTCPrecisions_TestGA,classifierNames);
saveas(gcf,'box_top9BGA50runsNTCP','jpeg');

%Tumor Class Precision- test
figure(11)
xlabel('Classifier');
ylabel('Tumor Class Precision');
title('Tumor Class Precision Using Top-9 features from BGA');
boxplot(TCPrecisions_TestGA,classifierNames);
saveas(gcf,'box_top9BGA50runsTCP','jpeg');

%Fmeasure- test
figure(12)
xlabel('Classifier');
ylabel('FMeasure');
title('FMeasure Using Top-9 features from BGA');
boxplot(FMeasures_TestGA,classifierNames);
saveas(gcf,'box_top9BGA50runsFmeasure','jpeg');