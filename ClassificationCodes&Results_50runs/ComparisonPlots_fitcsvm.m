%-----------------------Preparing Test Data---------------------------------
Accuracies_Test11runsAF = csvread('Accuracies_Test11runsAF.csv');
Accuracies_Test11runsGA8 = csvread('Accuracies_Test11runsGA8.csv');
Accuracies_Test11runsGA9 = csvread('Accuracies_Test11runsGA9.csv');
Accuracies_Test11runsGA10 = csvread('Accuracies_Test11runsGA10.csv');
Accuracies_Test11runsGA11 = csvread('Accuracies_Test11runsGA11.csv');
Accuracies_Test11runsPSO8 = csvread('Accuracies_Test11runsPSO8.csv');
Accuracies_Test11runsPSO9 = csvread('Accuracies_Test11runsPSO9.csv');
Accuracies_Test11runsPSO10 = csvread('Accuracies_Test11runsPSO10.csv');
Accuracies_Test11runsPSO11 = csvread('Accuracies_Test11runsPSO11.csv');
Accuracies_Test50runsGWO8 = csvread('Accuracies_Test50runsGWO8.csv');
Accuracies_Test50runsGWO9 = csvread('Accuracies_Test50runsGWO9.csv');
Accuracies_Test50runsGWO10 = csvread('Accuracies_Test50runsGWO10.csv');
Accuracies_Test50runsGWO11 = csvread('Accuracies_Test50runsGWO11.csv');
%-----------------------Preparing Train Data---------------------------------
Accuracies_Train11runsAF = csvread('Accuracies_Train11runsAF.csv');
Accuracies_Train11runsGA8 = csvread('Accuracies_Train11runsGA8.csv');
Accuracies_Train11runsGA9 = csvread('Accuracies_Train11runsGA9.csv');
Accuracies_Train11runsGA10 = csvread('Accuracies_Train11runsGA10.csv');
Accuracies_Train11runsGA11 = csvread('Accuracies_Train11runsGA11.csv');
Accuracies_Train11runsPSO8 = csvread('Accuracies_Train11runsPSO8.csv');
Accuracies_Train11runsPSO9 = csvread('Accuracies_Train11runsPSO9.csv');
Accuracies_Train11runsPSO10 = csvread('Accuracies_Train11runsPSO10.csv');
Accuracies_Train11runsPSO11 = csvread('Accuracies_Train11runsPSO11.csv');
Accuracies_Train50runsGWO8 = csvread('Accuracies_Train50runsGWO8.csv');
Accuracies_Train50runsGWO9 = csvread('Accuracies_Train50runsGWO9.csv');
Accuracies_Train50runsGWO10 = csvread('Accuracies_Train50runsGWO10.csv');
Accuracies_Train50runsGWO11 = csvread('Accuracies_Train50runsGWO11.csv');

classifierNames = ['KNN','DA-pl','DA-dl','NB-n','NB-k','DT','SVM-l','SVM-r','SVM-p2','RF'];
diffFeatures = {'AF','BGA-8','BGA-9','BGA-10','BGA-11','BPSO-8','BPSO-9','BPSO-10','BPSO-11','BGWO-8','BGWO-9','BGWO-10','BGWO-11'};

%----------------------------------------------------------------------
%--------------------Accuracies Training-------------------------------
%----------------------------------------------------------------------
%All Accuracies for KNN classifier with Testing Set
AccTestKNN = Accuracies_Test11runsAF(:,1);
AccTestKNN = [AccTestKNN,Accuracies_Test11runsGA8(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsGA9(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsGA10(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsGA11(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsPSO8(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsPSO9(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsPSO10(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test11runsPSO11(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test50runsGWO8(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test50runsGWO9(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test50runsGWO10(:,1)];
AccTestKNN = [AccTestKNN,Accuracies_Test50runsGWO11(:,1)];

figure()
boxplot(AccTestKNN,diffFeatures);
title('Classification Accuracy with KNN using different features-(Test-Data)');
ylabel('Accurcies with KNN');
xlabel('Different Features');
savefig('KNNAccuracytest.fig');

%All Accuracies for DA-pl classifier with Testing Set
AccTestDA-pl = Accuracies_Test11runsAF(:,2);
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test11runsGA8(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test11runsGA9(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test11runsGA10(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test11runsGA11(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test11runsPSO8(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test11runsPSO9(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test11runsPSO10(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test11runsPSO11(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test50runsGWO8(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test50runsGWO9(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test50runsGWO10(:,2)];
AccTestDA-pl = [AccTestDA-pl,Accuracies_Test50runsGWO11(:,2)];

figure()
boxplot(AccTestDA-pl,diffFeatures);
title('Classification Accuracy with DA-pl using different features-(Test-Data)');
ylabel('Accurcies with DA-pl');
xlabel('Different Features');
savefig('DA-plAccuracytest.fig');

%All Accuracies for DA-dl classifier with Testing Set
AccTestDA-dl = Accuracies_Test11runsAF(:,3);
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test11runsGA8(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test11runsGA9(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test11runsGA10(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test11runsGA11(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test11runsPSO8(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test11runsPSO9(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test11runsPSO10(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test11runsPSO11(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test50runsGWO8(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test50runsGWO9(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test50runsGWO10(:,3)];
AccTestDA-dl = [AccTestDA-dl,Accuracies_Test50runsGWO11(:,3)];

figure()
boxplot(AccTestDA-dl,diffFeatures);
title('Classification Accuracy with DA-dl using different features-(Test-Data)');
ylabel('Accurcies with DA-dl');
xlabel('Different Features');
savefig('DA-dlAccuracytest.fig');

%All Accuracies for NB-n classifier with Testing Set
AccTestNB-n = Accuracies_Test11runsAF(:,4);
AccTestNB-n = [AccTestNB-n,Accuracies_Test11runsGA8(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test11runsGA9(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test11runsGA10(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test11runsGA11(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test11runsPSO8(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test11runsPSO9(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test11runsPSO10(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test11runsPSO11(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test50runsGWO8(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test50runsGWO9(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test50runsGWO10(:,4)];
AccTestNB-n = [AccTestNB-n,Accuracies_Test50runsGWO11(:,4)];

figure()
boxplot(AccTestNB-n,diffFeatures);
title('Classification Accuracy with NB-n using different features-(Test-Data)');
ylabel('Accurcies with NB-n');
xlabel('Different Features');
savefig('NB-nAccuracytest.fig');

%All Accuracies for NB-k classifier with Testing Set
AccTestNB-k = Accuracies_Test11runsAF(:,5);
AccTestNB-k = [AccTestNB-k,Accuracies_Test11runsGA8(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test11runsGA9(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test11runsGA10(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test11runsGA11(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test11runsPSO8(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test11runsPSO9(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test11runsPSO10(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test11runsPSO11(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test50runsGWO8(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test50runsGWO9(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test50runsGWO10(:,5)];
AccTestNB-k = [AccTestNB-k,Accuracies_Test50runsGWO11(:,5)];

figure()
boxplot(AccTestNB-k,diffFeatures);
title('Classification Accuracy with NB-k using different features-(Test-Data)');
ylabel('Accurcies with NB-k');
xlabel('Different Features');
savefig('NB-kAccuracytest.fig');

%All Accuracies for DT classifier with Testing Set
AccTestDT = Accuracies_Test11runsAF(:,6);
AccTestDT = [AccTestDT,Accuracies_Test11runsGA8(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsGA9(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsGA10(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsGA11(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsPSO8(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsPSO9(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsPSO10(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test11runsPSO11(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test50runsGWO8(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test50runsGWO9(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test50runsGWO10(:,6)];
AccTestDT = [AccTestDT,Accuracies_Test50runsGWO11(:,6)];

figure()
boxplot(AccTestDT,diffFeatures);
title('Classification Accuracy with DT using different features-(Test-Data)');
ylabel('Accurcies with DT');
xlabel('Different Features');
savefig('DTAccuracytest.fig');

%All Accuracies for SVM-l classifier with Testing Set
AccTestSVM-l = Accuracies_Test11runsAF(:,7);
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test11runsGA8(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test11runsGA9(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test11runsGA10(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test11runsGA11(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test11runsPSO8(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test11runsPSO9(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test11runsPSO10(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test11runsPSO11(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test50runsGWO8(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test50runsGWO9(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test50runsGWO10(:,7)];
AccTestSVM-l = [AccTestSVM-l,Accuracies_Test50runsGWO11(:,7)];

figure()
boxplot(AccTestSVM-l,diffFeatures);
title('Classification Accuracy with SVM-l using different features-(Test-Data)');
ylabel('Accurcies with SVM-l');
xlabel('Different Features');
savefig('SVM-lAccuracytest.fig');

%All Accuracies for SVM-r classifier with Testing Set
AccTestSVM-r = Accuracies_Test11runsAF(:,8);
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test11runsGA8(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test11runsGA9(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test11runsGA10(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test11runsGA11(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test11runsPSO8(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test11runsPSO9(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test11runsPSO10(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test11runsPSO11(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test50runsGWO8(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test50runsGWO9(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test50runsGWO10(:,8)];
AccTestSVM-r = [AccTestSVM-r,Accuracies_Test50runsGWO11(:,8)];

figure()
boxplot(AccTestSVM-r,diffFeatures);
title('Classification Accuracy with SVM-r using different features-(Test-Data)');
ylabel('Accurcies with SVM-r');
xlabel('Different Features');
savefig('SVM-rAccuracytest.fig');

%All Accuracies for SVM-p2 classifier with Testing Set
AccTestSVM-p2 = Accuracies_Test11runsAF(:,9);
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test11runsGA8(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test11runsGA9(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test11runsGA10(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test11runsGA11(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test11runsPSO8(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test11runsPSO9(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test11runsPSO10(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test11runsPSO11(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test50runsGWO8(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test50runsGWO9(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test50runsGWO10(:,9)];
AccTestSVM-p2 = [AccTestSVM-p2,Accuracies_Test50runsGWO11(:,9)];

figure()
boxplot(AccTestSVM-p2,diffFeatures);
title('Classification Accuracy with SVM-p2 using different features-(Test-Data)');
ylabel('Accurcies with SVM-p2');
xlabel('Different Features');
savefig('SVM-p2Accuracytest.fig');

%All Accuracies for RF classifier with Testing Set
AccTestRF = Accuracies_Test11runsAF(:,10);
AccTestRF = [AccTestRF,Accuracies_Test11runsGA8(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsGA9(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsGA10(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsGA11(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsPSO8(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsPSO9(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsPSO10(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test11runsPSO11(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test50runsGWO8(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test50runsGWO9(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test50runsGWO10(:,10)];
AccTestRF = [AccTestRF,Accuracies_Test50runsGWO11(:,10)];

figure()
boxplot(AccTestRF,diffFeatures);
title('Classification Accuracy with RF using different features-(Test-Data)');
ylabel('Accurcies with RF');
xlabel('Different Features');
savefig('RFAccuracytest.fig');

%----------------------------------------------------------------------
%--------------------Accuracies Training-------------------------------
%----------------------------------------------------------------------

%All Accuracies for KNN classifier with Training Set
AccTrainKNN = Accuracies_Train11runsAF(:,1);
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsGA8(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsGA9(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsGA10(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsGA11(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsPSO8(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsPSO9(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsPSO10(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train11runsPSO11(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train50runsGWO8(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train50runsGWO9(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train50runsGWO10(:,1)];
AccTrainKNN = [AccTrainKNN,Accuracies_Train50runsGWO11(:,1)];

figure()
boxplot(AccTrainKNN,diffFeatures);
title('Classification Accuracy with KNN using different features-(Train-Data)');
ylabel('Accurcies with KNN');
xlabel('Different Features');
savefig('KNNAccuracytrain.fig');

%All Accuracies for DA-pl classifier with Training Set
AccTrainDA-pl = Accuracies_Train11runsAF(:,2);
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train11runsGA8(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train11runsGA9(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train11runsGA10(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train11runsGA11(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train11runsPSO8(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train11runsPSO9(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train11runsPSO10(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train11runsPSO11(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train50runsGWO8(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train50runsGWO9(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train50runsGWO10(:,2)];
AccTrainDA-pl = [AccTrainDA-pl,Accuracies_Train50runsGWO11(:,2)];

figure()
boxplot(AccTrainDA-pl,diffFeatures);
title('Classification Accuracy with DA-pl using different features-(Train-Data)');
ylabel('Accurcies with DA-pl');
xlabel('Different Features');
savefig('DA-plAccuracytrain.fig');

%All Accuracies for DA-dl classifier with Training Set
AccTrainDA-dl = Accuracies_Train11runsAF(:,3);
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train11runsGA8(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train11runsGA9(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train11runsGA10(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train11runsGA11(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train11runsPSO8(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train11runsPSO9(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train11runsPSO10(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train11runsPSO11(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train50runsGWO8(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train50runsGWO9(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train50runsGWO10(:,3)];
AccTrainDA-dl = [AccTrainDA-dl,Accuracies_Train50runsGWO11(:,3)];

figure()
boxplot(AccTrainDA-dl,diffFeatures);
title('Classification Accuracy with DA-dl using different features-(Train-Data)');
ylabel('Accurcies with DA-dl');
xlabel('Different Features');
savefig('DA-dlAccuracytrain.fig');

%All Accuracies for NB-n classifier with Training Set
AccTrainNB-n = Accuracies_Train11runsAF(:,4);
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train11runsGA8(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train11runsGA9(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train11runsGA10(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train11runsGA11(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train11runsPSO8(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train11runsPSO9(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train11runsPSO10(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train11runsPSO11(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train50runsGWO8(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train50runsGWO9(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train50runsGWO10(:,4)];
AccTrainNB-n = [AccTrainNB-n,Accuracies_Train50runsGWO11(:,4)];

figure()
boxplot(AccTrainNB-n,diffFeatures);
title('Classification Accuracy with NB-n using different features-(Train-Data)');
ylabel('Accurcies with NB-n');
xlabel('Different Features');
savefig('NB-nAccuracytrain.fig');

%All Accuracies for NB-k classifier with Training Set
AccTrainNB-k = Accuracies_Train11runsAF(:,5);
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train11runsGA8(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train11runsGA9(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train11runsGA10(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train11runsGA11(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train11runsPSO8(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train11runsPSO9(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train11runsPSO10(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train11runsPSO11(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train50runsGWO8(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train50runsGWO9(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train50runsGWO10(:,5)];
AccTrainNB-k = [AccTrainNB-k,Accuracies_Train50runsGWO11(:,5)];

figure()
boxplot(AccTrainNB-k,diffFeatures);
title('Classification Accuracy with NB-k using different features-(Train-Data)');
ylabel('Accurcies with NB-k');
xlabel('Different Features');
savefig('NB-kAccuracytrain.fig');

%All Accuracies for DT classifier with Training Set
AccTrainDT = Accuracies_Train11runsAF(:,6);
AccTrainDT = [AccTrainDT,Accuracies_Train11runsGA8(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsGA9(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsGA10(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsGA11(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsPSO8(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsPSO9(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsPSO10(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train11runsPSO11(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train50runsGWO8(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train50runsGWO9(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train50runsGWO10(:,6)];
AccTrainDT = [AccTrainDT,Accuracies_Train50runsGWO11(:,6)];

figure()
boxplot(AccTrainDT,diffFeatures);
title('Classification Accuracy with DT using different features-(Train-Data)');
ylabel('Accurcies with DT');
xlabel('Different Features');
savefig('DTAccuracytrain.fig');

%All Accuracies for SVM-l classifier with Training Set
AccTrainSVM-l = Accuracies_Train11runsAF(:,7);
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train11runsGA8(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train11runsGA9(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train11runsGA10(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train11runsGA11(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train11runsPSO8(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train11runsPSO9(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train11runsPSO10(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train11runsPSO11(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train50runsGWO8(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train50runsGWO9(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train50runsGWO10(:,7)];
AccTrainSVM-l = [AccTrainSVM-l,Accuracies_Train50runsGWO11(:,7)];

figure()
boxplot(AccTrainSVM-l,diffFeatures);
title('Classification Accuracy with SVM-l using different features-(Train-Data)');
ylabel('Accurcies with SVM-l');
xlabel('Different Features');
savefig('SVM-lAccuracytrain.fig');

%All Accuracies for SVM-r classifier with Training Set
AccTrainSVM-r = Accuracies_Train11runsAF(:,8);
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train11runsGA8(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train11runsGA9(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train11runsGA10(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train11runsGA11(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train11runsPSO8(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train11runsPSO9(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train11runsPSO10(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train11runsPSO11(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train50runsGWO8(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train50runsGWO9(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train50runsGWO10(:,8)];
AccTrainSVM-r = [AccTrainSVM-r,Accuracies_Train50runsGWO11(:,8)];

figure()
boxplot(AccTrainSVM-r,diffFeatures);
title('Classification Accuracy with SVM-r using different features-(Train-Data)');
ylabel('Accurcies with SVM-r');
xlabel('Different Features');
savefig('SVM-rAccuracytrain.fig');

%All Accuracies for SVM-p2 classifier with Training Set
AccTrainSVM-p2 = Accuracies_Train11runsAF(:,9);
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train11runsGA8(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train11runsGA9(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train11runsGA10(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train11runsGA11(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train11runsPSO8(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train11runsPSO9(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train11runsPSO10(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train11runsPSO11(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train50runsGWO8(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train50runsGWO9(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train50runsGWO10(:,9)];
AccTrainSVM-p2 = [AccTrainSVM-p2,Accuracies_Train50runsGWO11(:,9)];

figure()
boxplot(AccTrainSVM-p2,diffFeatures);
title('Classification Accuracy with SVM-p2 using different features-(Train-Data)');
ylabel('Accurcies with SVM-p2');
xlabel('Different Features');
savefig('SVM-p2Accuracytrain.fig');

%All Accuracies for RF classifier with Training Set
AccTrainRF = Accuracies_Train11runsAF(:,10);
AccTrainRF = [AccTrainRF,Accuracies_Train11runsGA8(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsGA9(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsGA10(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsGA11(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsPSO8(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsPSO9(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsPSO10(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train11runsPSO11(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train50runsGWO8(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train50runsGWO9(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train50runsGWO10(:,10)];
AccTrainRF = [AccTrainRF,Accuracies_Train50runsGWO11(:,10)];

figure()
boxplot(AccTrainRF,diffFeatures);
title('Classification Accuracy with RF using different features-(Train-Data)');
ylabel('Accurcies with RF');
xlabel('Different Features');
savefig('RFAccuracytrain.fig');
