clear; close all; clc;
%load dataset
% Running all classifiers for 10 times
trainingIndex = [];
trainingIndex = logical(trainingIndex);
testingIndex = [];
testingIndex = logical(testingIndex);
for j = 1:50
    fprintf('Iteration number: %d \n',j);
    load featMatrix.mat;

    % Diving data into testing & training sets
    c = cvpartition(lmat,'HoldOut',0.30); % 30% testing and 70% training
    trainidx = training(c); % generates logical vector of training index
    testidx = test(c); % generates logical vector of testing index
    
    %store trainidx and testidx into global variables
    trainingIndex = [trainingIndex trainidx];
    testingIndex = [testingIndex testidx];
    
% load partition indexes
% load trainidx7030.mat
% load testidx7030.mat

    %Original data with all features
    traindata = featMatrix(trainidx,:);
    testdata = featMatrix(testidx,:);
    %training Data
    trainlabel = traindata(:,14);
    trainfeat = traindata(:,1:13);
    %test Data
    testlabel = testdata(:,14);
    testfeat = testdata(:,1:13);

%-----------------------------------Applying Classifiers with all features-----------

    
    % (1) Perform k-nearest neighbor (KNN)
    disp('KNN')
    kfold=10; k=3; % k-value in KNN
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jKNN(trainlabel,trainfeat,testlabel,testfeat,k,kfold); 
    %Save KNN evaluation metrics into vectors
    %testing metrics
    Accuracy_Test_KNN(j) = AccuracyTest;
    Recall_Test_KNN(j) = RecallTest;
    Specificity_Test_KNN(j) = SpecificityTest;
    PPV_Test_KNN(j) = PPVTest;
    NPV_Test_KNN(j) = NPVTest;
    FMeasure_Test_KNN(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_KNN(j) = AccuracyTrain;
    Recall_Train_KNN(j) = RecallTrain;
    Specificity_Train_KNN(j) = SpecificityTrain;
    PPV_Train_KNN(j) = PPVTrain;
    NPV_Train_KNN(j) = NPVTrain;
    FMeasure_Train_KNN(j) = FMeasureTrain;
    
    % (2) Perform discriminate analysis (DA)
%     disp("*********DA*********")
    kfold=10;
    % The Discriminate can selected as follows:
    % 'l' : linear 
    % 'q' : quadratic
    % 'pq': pseudoquadratic
    % 'pl': pseudolinear
    % 'dl': diaglinear
    % 'dq': diagquadratic
    Disc = {'l','q','pq','pl','dl','dq'};
    for i= 4:5
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDA(trainlabel,trainfeat,testlabel,testfeat,Disc{i},kfold,i);
        if(i==4)
            disp('DA-psuedolinear')
            %testing metrics
            Accuracy_Test_DAdl(j) = AccuracyTest;
            Recall_Test_DAdl(j) = RecallTest;
            Specificity_Test_DAdl(j) = SpecificityTest;
            PPV_Test_DAdl(j) = PPVTest;
            NPV_Test_DAdl(j) = NPVTest;
            FMeasure_Test_DAdl(j) = FMeasureTest;
            
            %training metrics
            Accuracy_Train_DAdl(j) = AccuracyTrain;
            Recall_Train_DAdl(j) = RecallTrain;
            Specificity_Train_DAdl(j) = SpecificityTrain;
            PPV_Train_DAdl(j) = PPVTrain;
            NPV_Train_DAdl(j) = NPVTrain;
            FMeasure_Train_DAdl(j) = FMeasureTrain;
            
        end
        if(i==5)
            disp('DA-diaglinear')
            %testing metrics
            Accuracy_Test_DAdq(j) = AccuracyTest;
            Recall_Test_DAdq(j) = RecallTest;
            Specificity_Test_DAdq(j) = SpecificityTest;
            PPV_Test_DAdq(j) = PPVTest;
            NPV_Test_DAdq(j) = NPVTest;
            FMeasure_Test_DAdq(j) = FMeasureTest;
            
            %training metrics
            Accuracy_Train_DAdq(j) = AccuracyTrain;
            Recall_Train_DAdq(j) = RecallTrain;
            Specificity_Train_DAdq(j) = SpecificityTrain;
            PPV_Train_DAdq(j) = PPVTrain;
            NPV_Train_DAdq(j) = NPVTrain;
            FMeasure_Train_DAdq(j) = FMeasureTrain;
        end
    end

    % (3) Perform Naive Bayes (NB)
%     disp("*********NB*********")
    kfold=10; Dist=['n','k']; 
    % The Distribution can selected as follows:
    % 'n' : normal distribution 
    % 'k' : kernel distribution
    for i= 1:2
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jNB(trainlabel,trainfeat,testlabel,testfeat,Dist(i),kfold,i); 
        if(i==1)
            disp('NB-normal')
            %testing metrics
            Accuracy_Test_NBn(j) = AccuracyTest;
            Recall_Test_NBn(j) = RecallTest;
            Specificity_Test_NBn(j) = SpecificityTest;
            PPV_Test_NBn(j) = PPVTest;
            NPV_Test_NBn(j) = NPVTest;
            FMeasure_Test_NBn(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_NBn(j) = AccuracyTrain;
            Recall_Train_NBn(j) = RecallTrain;
            Specificity_Train_NBn(j) = SpecificityTrain;
            PPV_Train_NBn(j) = PPVTrain;
            NPV_Train_NBn(j) = NPVTrain;
            FMeasure_Train_NBn(j) = FMeasureTrain;
        end
        if(i==2)
            disp('NB-kernel')
            %testing metrics
            Accuracy_Test_NBk(j) = AccuracyTest;
            Recall_Test_NBk(j) = RecallTest;
            Specificity_Test_NBk(j) = SpecificityTest;
            PPV_Test_NBk(j) = PPVTest;
            NPV_Test_NBk(j) = NPVTest;
            FMeasure_Test_NBk(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_NBk(j) = AccuracyTrain;
            Recall_Train_NBk(j) = RecallTrain;
            Specificity_Train_NBk(j) = SpecificityTrain;
            PPV_Train_NBk(j) = PPVTrain;
            NPV_Train_NBk(j) = NPVTrain;
            FMeasure_Train_NBk(j) = FMeasureTrain;
        end
    end

    % (4) Perform decision tree (DT)
    disp('DT')
    kfold=10; nSplit=50; % Number of split in DT
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDT(trainlabel,trainfeat,testlabel,testfeat,nSplit,kfold);
    %testing metrics
    Accuracy_Test_DT(j) = AccuracyTest;
    Recall_Test_DT(j) = RecallTest;
    Specificity_Test_DT(j) = SpecificityTest;
    PPV_Test_DT(j) = PPVTest;
    NPV_Test_DT(j) = NPVTest;
    FMeasure_Test_DT(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_DT(j) = AccuracyTrain;
    Recall_Train_DT(j) = RecallTrain;
    Specificity_Train_DT(j) = SpecificityTrain;
    PPV_Train_DT(j) = PPVTrain;
    NPV_Train_DT(j) = NPVTrain;
    FMeasure_Train_DT(j) = FMeasureTrain;

    % (5) Perform support vector machine
    kernel = ['l','r','p'];
%     disp("*********SVM*********")
    for i= 1:3
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jSVM(trainlabel,trainfeat,testlabel,testfeat,kernel(i),i);
        if(i==1)
            disp('SVM-linear')
            %testing metrics
            Accuracy_Test_SVMl(j) = AccuracyTest;
            Recall_Test_SVMl(j) = RecallTest;
            Specificity_Test_SVMl(j) = SpecificityTest;
            PPV_Test_SVMl(j) = PPVTest;
            NPV_Test_SVMl(j) = NPVTest;
            FMeasure_Test_SVMl(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMl(j) = AccuracyTrain;
            Recall_Train_SVMl(j) = RecallTrain;
            Specificity_Train_SVMl(j) = SpecificityTrain;
            PPV_Train_SVMl(j) = PPVTrain;
            NPV_Train_SVMl(j) = NPVTrain;
            FMeasure_Train_SVMl(j) = FMeasureTrain;
        end
        if(i==2)
            disp('SVM-Gaussian Radial Basis function')
            %testing metrics
            Accuracy_Test_SVMr(j) = AccuracyTest;
            Recall_Test_SVMr(j) = RecallTest;
            Specificity_Test_SVMr(j) = SpecificityTest;
            PPV_Test_SVMr(j) = PPVTest;
            NPV_Test_SVMr(j) = NPVTest;
            FMeasure_Test_SVMr(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMr(j) = AccuracyTrain;
            Recall_Train_SVMr(j) = RecallTrain;
            Specificity_Train_SVMr(j) = SpecificityTrain;
            PPV_Train_SVMr(j) = PPVTrain;
            NPV_Train_SVMr(j) = NPVTrain;
            FMeasure_Train_SVMr(j) = FMeasureTrain;
        end
        if(i==3)
            disp('SVM-Polinomial-Degree2')
            %testing metrics
            Accuracy_Test_SVMp(j) = AccuracyTest;
            Recall_Test_SVMp(j) = RecallTest;
            Specificity_Test_SVMp(j) = SpecificityTest;
            PPV_Test_SVMp(j) = PPVTest;
            NPV_Test_SVMp(j) = NPVTest;
            FMeasure_Test_SVMp(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMp(j) = AccuracyTrain;
            Recall_Train_SVMp(j) = RecallTrain;
            Specificity_Train_SVMp(j) = SpecificityTrain;
            PPV_Train_SVMp(j) = PPVTrain;
            NPV_Train_SVMp(j) = NPVTrain;
            FMeasure_Train_SVMp(j) = FMeasureTrain;
        end
    end
    % 'r' : radial basis function  
    % 'l' : linear function 
    % 'p' : polynomial function 
    % 'g' : gaussian function

    % (6) Perform random forest (RF)
    disp('RF')
    kfold=10; nBag=50; % Number of bags in RF
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jRF(trainlabel,trainfeat,testlabel,testfeat,nBag,kfold);
    %testing metrics
    Accuracy_Test_RF(j) = AccuracyTest;
    Recall_Test_RF(j) = RecallTest;
    Specificity_Test_RF(j) = SpecificityTest;
    PPV_Test_RF(j) = PPVTest;
    NPV_Test_RF(j) = NPVTest;
    FMeasure_Test_RF(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_RF(j) = AccuracyTrain;
    Recall_Train_RF(j) = RecallTrain;
    Specificity_Train_RF(j) = SpecificityTrain;
    PPV_Train_RF(j) = PPVTrain;
    NPV_Train_RF(j) = NPVTrain;
    FMeasure_Train_RF(j) = FMeasureTrain;
    
    %Run Mainfor11itrGAfeatTop8.m
    
    
end
Accuracies_Test = [Accuracy_Test_KNN;Accuracy_Test_DAdl;Accuracy_Test_DAdq;Accuracy_Test_NBn;Accuracy_Test_NBk;Accuracy_Test_DT;Accuracy_Test_SVMl;Accuracy_Test_SVMr;Accuracy_Test_SVMp;Accuracy_Test_RF];
Accuracies_Test = Accuracies_Test';
Accuracies_Test_mean = mean(Accuracies_Test);
Accuracies_Train = [Accuracy_Train_KNN;Accuracy_Train_DAdl;Accuracy_Train_DAdq;Accuracy_Train_NBn;Accuracy_Train_NBk;Accuracy_Train_DT;Accuracy_Train_SVMl;Accuracy_Train_SVMr;Accuracy_Train_SVMp;Accuracy_Train_RF];
Accuracies_Train = Accuracies_Train';
Accuracies_Train_mean = mean(Accuracies_Train);
Recalls_Test = [Recall_Test_KNN;Recall_Test_DAdl;Recall_Test_DAdq;Recall_Test_NBn;Recall_Test_NBk; Recall_Test_DT;Recall_Test_SVMl;Recall_Test_SVMr;Recall_Test_SVMp;Recall_Test_RF];
Recalls_Test = Recalls_Test';
Recalls_Test_mean = mean(Recalls_Test);
Recalls_Train = [Recall_Train_KNN;Recall_Train_DAdl;Recall_Train_DAdq;Recall_Train_NBn;Recall_Train_NBk;Recall_Train_DT; Recall_Train_SVMl;Recall_Train_SVMr;Recall_Train_SVMp;Recall_Train_RF];
Recalls_Train = Recalls_Train';
Recalls_Train_mean = mean(Recalls_Train);
Specificities_Test = [Specificity_Test_KNN;Specificity_Test_DAdl;Specificity_Test_DAdq;Specificity_Test_NBn;Specificity_Test_NBk;Specificity_Test_DT;Specificity_Test_SVMl;Specificity_Test_SVMr;Specificity_Test_SVMp;Specificity_Test_RF];
Specificities_Test = Specificities_Test';
Specificities_Test_mean = mean(Specificities_Test);
Specificities_Train = [Specificity_Train_KNN;Specificity_Train_DAdl;Specificity_Train_DAdq;Specificity_Train_NBn;Specificity_Train_NBk;Specificity_Train_DT;Specificity_Train_SVMl;Specificity_Train_SVMr;Specificity_Train_SVMp;Specificity_Train_RF];
Specificities_Train = Specificities_Train';
Specificities_Train_mean = mean(Specificities_Train);
NTCPrecisions_Test = [PPV_Test_KNN;PPV_Test_DAdl;PPV_Test_DAdq;PPV_Test_NBn;PPV_Test_NBk;PPV_Test_DT;PPV_Test_SVMl;PPV_Test_SVMr;PPV_Test_SVMp;PPV_Test_RF];
NTCPrecisions_Test = NTCPrecisions_Test';
NTCPrecisions_Test_mean = mean(NTCPrecisions_Test);
NTCPrecisions_Train = [PPV_Train_KNN;PPV_Train_DAdl;PPV_Train_DAdq;PPV_Train_NBn;PPV_Train_NBk;PPV_Train_DT;PPV_Train_SVMl;PPV_Train_SVMr;PPV_Train_SVMp;PPV_Train_RF];
NTCPrecisions_Train = NTCPrecisions_Train';
NTCPrecisions_Train_mean = mean(NTCPrecisions_Train);
TCPrecisions_Test = [NPV_Test_KNN;NPV_Test_DAdl;NPV_Test_DAdq;NPV_Test_NBn;NPV_Test_NBk;NPV_Test_DT;NPV_Test_SVMl;NPV_Test_SVMr;NPV_Test_SVMp;NPV_Test_RF];
TCPrecisions_Test = TCPrecisions_Test';
TCPrecisions_Test_mean = mean(TCPrecisions_Test);
TCPrecisions_Train = [NPV_Train_KNN;NPV_Train_DAdl;NPV_Train_DAdq;NPV_Train_NBn;NPV_Train_NBk;NPV_Train_DT;NPV_Train_SVMl;NPV_Train_SVMr;NPV_Train_SVMp;NPV_Train_RF];
TCPrecisions_Train = TCPrecisions_Train';
TCPrecisions_Train_mean = mean(TCPrecisions_Train);
FMeasures_Test = [FMeasure_Test_KNN;FMeasure_Test_DAdl;FMeasure_Test_DAdq;FMeasure_Test_NBn;FMeasure_Test_NBk;FMeasure_Test_DT;FMeasure_Test_SVMl;FMeasure_Test_SVMr;FMeasure_Test_SVMp;FMeasure_Test_RF];
FMeasures_Test = FMeasures_Test';
FMeasures_Test_mean = mean(FMeasures_Test);
FMeasures_Train = [FMeasure_Train_KNN;FMeasure_Train_DAdl;FMeasure_Train_DAdq;FMeasure_Train_NBn;FMeasure_Train_NBk;FMeasure_Train_DT;FMeasure_Train_SVMl;FMeasure_Train_SVMr;FMeasure_Train_SVMp;FMeasure_Train_RF];
FMeasures_Train = FMeasures_Train';
FMeasures_Train_mean = mean(FMeasures_Train);
MeanMetrics_Test = [Accuracies_Test_mean;Recalls_Test_mean;Specificities_Test_mean;NTCPrecisions_Test_mean;TCPrecisions_Test_mean;FMeasures_Test_mean];
MeanMetrics_Train = [Accuracies_Train_mean;Recalls_Train_mean;Specificities_Train_mean;NTCPrecisions_Train_mean;TCPrecisions_Train_mean;FMeasures_Train_mean];
%save results
csvwrite('Accuracies_Test11runsAF.csv',Accuracies_Test);
csvwrite('Accuracies_Train11runsAF.csv',Accuracies_Train);
csvwrite('Recalls_Test11runsAF.csv',Recalls_Test);
csvwrite('Recalls_Train11runsAF.csv',Recalls_Train);
csvwrite('Specificities_Test11runsAF.csv',Specificities_Test);
csvwrite('Specificities_Train11runsAF.csv',Specificities_Train);
csvwrite('NTCPrecisions_Test11runsAF.csv',NTCPrecisions_Test);
csvwrite('NTCPrecisions_Train11runsAF.csv',NTCPrecisions_Train);
csvwrite('TCPrecisions_Test11runsAF.csv',TCPrecisions_Test);
csvwrite('TCPrecisions_Train11runsAF.csv',TCPrecisions_Train);
csvwrite('FMeasures_Test11runsAF.csv',FMeasures_Test);
csvwrite('FMeasures_Train11runsAF.csv',FMeasures_Train);
csvwrite('MeanMetrics_Test11runsAF.csv',MeanMetrics_Test);
csvwrite('MeanMetrics_Train11runsAF.csv',MeanMetrics_Train);

%save global indexs
% csvwrite('trainingIndex.csv',trainingIndex);
% csvwrite('testingIndex.csv',testingIndex);
save testingIndex7030.mat testingIndex
save trainingIndex7030.mat trainingIndex

%plot figures for Test data set
classifierNames = categorical({'KNN','DA-pl','DA-dl','NB-n','NB-k','DT','SVM-l','SVM-r','SVM-p2','RF'});
%Accuracies barh graphs
figure(1)
barh(classifierNames,Accuracies_Test_mean)
title('Accuracy using All features');
xlabel('Accuracy');
saveas(gcf,'allFeatureAccuracy50runs','jpeg');

%Recalls barh graphs
figure(2)
barh(classifierNames,Recalls_Test_mean)
title('Recall using All features');
xlabel('Recall');
saveas(gcf,'allFeatureRecall50runs','jpeg');

%Specificities barh graphs
figure(3)
barh(classifierNames,Specificities_Test_mean)
title('Specificity using All features');
xlabel('Specificity');
saveas(gcf,'allFeatureSpecificity50runs','jpeg');

%NTCPrecisions barh graphs
figure(4)
barh(classifierNames,NTCPrecisions_Test_mean)
title('Non Tumor Class Precision using All features');
xlabel('Non Tumor Class Precision');
saveas(gcf,'allFeatureNTCP50runs','jpeg');

%TCPrecisions barh graphs
figure(5)
barh(classifierNames,TCPrecisions_Test_mean)
title('Tumor Class Precision using All features');
xlabel('Tumor Class Precision');
saveas(gcf,'allFeatureTCP50runs','jpeg');

%FMeasures barh graphs
figure(6)
barh(classifierNames,FMeasures_Test_mean)
title('FMeasure using All features');
xlabel('FMeasure');
saveas(gcf,'allFeatureFmeasure50runs','jpeg');

%BoxPlot for all the evaluation metrics for all the classifiers
%Accuracy- Testset
figure(7)
xlabel('Classifier');
ylabel('Accuracy');
title('Classification Accuracy Using All Features');
boxplot(Accuracies_Test,classifierNames);
saveas(gcf,'allFeature50runsboxplotAccuracy','jpeg');

%Recall- Testset
figure(8)
xlabel('Classifier');
ylabel('Recall');
title('Recall Using All Features');
boxplot(Recalls_Test,classifierNames);
saveas(gcf,'allFeature50runsboxplotRecall','jpeg');

%Specificities- test
figure(9)
xlabel('Classifier');
ylabel('Specificity');
title('Specificity Using All Features');
boxplot(Specificities_Test,classifierNames);
saveas(gcf,'allFeature50runsboxplotSpecificity','jpeg');

%Non Tumor Class Precision- test
figure(10)
xlabel('Classifier');
ylabel('Non Tumor Class Precision');
title('Non Tumor Class Precision Using All Features');
boxplot(NTCPrecisions_Test,classifierNames);
saveas(gcf,'allFeature50runsboxplotNTCP','jpeg');

%Tumor Class Precision- test
figure(11)
xlabel('Classifier');
ylabel('Tumor Class Precision');
title('Tumor Class Precision Using All Features');
boxplot(TCPrecisions_Test,classifierNames);
saveas(gcf,'allFeature50runsboxplotTCP','jpeg');

%Fmeasure- test
figure(12)
xlabel('Classifier');
ylabel('FMeasure');
title('FMeasure Using All Features');
boxplot(FMeasures_Test,classifierNames);
saveas(gcf,'allFeature50runsboxplotFMeasure','jpeg');








