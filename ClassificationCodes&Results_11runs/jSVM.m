%-------------------------------------------------------------------------%
%  Machine learning algorithms source codes demo version                  %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jSVM(trainlabel,trainfeat,testlabel,testfeat,kernelcode,i)
switch kernelcode
    case'l'; kernel='linear';     
    case'r'; kernel='rbf'; 
    case'p'; kernel='polynomial';   
end
rng('default');

% fprintf('------Kernel: %s \n',kernel)
%execute only if it is polynomial kernel
if kernelcode=='p'
    svmModel = fitcsvm(trainfeat,trainlabel,'KernelFunction',kernel,'PolynomialOrder',2,'KernelScale','auto','ClassNames',[0,1]);
end

%execute only if it is not polynomial kernel
if kernelcode~='p'
    svmModel = fitcsvm(trainfeat,trainlabel,'KernelFunction',kernel,'KernelScale','auto','ClassNames',[0,1]);
end

%classifying new data with trained SVM model for Testing and Training Sets
predictedTrain = predict(svmModel,trainfeat);
predictedTest = predict(svmModel,testfeat);

%Confusion matrix with Training and Testing sets
confmatTrain=confusionmat(trainlabel,predictedTrain);
confmatTest=confusionmat(testlabel,predictedTest);

% disp("Confusion Matrix Training set")
% disp(confmatTrain)
[AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest] = evalmetrics(confmatTrain);

% disp("Confusion Matrix Testing set")
% disp(confmatTest)
[AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = evalmetrics(confmatTest);

%plot confusion matrix for both Training and Testing sets
% figure(i*20)
% name = ['SVM-Testing-Set-',kernel,'-'];
% plotconfusion(testlabel',predictedTest',name);

% figure(i*21)
% name = ['SVM-Training-Set-',kernel,'-'];
% plotconfusion(trainlabel',predictedTrain',name);
end 

