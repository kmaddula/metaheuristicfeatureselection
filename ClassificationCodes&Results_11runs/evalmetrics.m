function [Accuracy, Recall, Specificity, PPV, NPV, FMeasure] = evalmetrics(a)

%clc

% Calculate Metrics

TP = a(1,1);
TN = a(2,2);
FP = a(1,2);
FN = a(2,1);

Total = TP + TN + FP + FN;

Accuracy = ((TP+TN)/Total)*100;
% disp('Accuracy')
% disp(Accuracy)
%Misclassification = ((FP+FN)/Total)*100
Recall = (TP /(TP+FN)) *100;
% disp('Recall')
% disp(Recall)
Specificity = (TN /(FP+TN)) *100;
% disp('Specificity')
% disp(Specificity)
PPV = ( TP/(TP+FP))*100;
% disp('PPV')
% disp(PPV)
NPV = ( TN/(TN+FN))*100;
% disp('NPV')
% disp(NPV)
FMeasure = (2*Recall*PPV)/(Recall+PPV);
% disp('FMeasure')
% disp(FMeasure)
%Error_Rate = 100 - Accuracy;
end
