%-------------------------------------------------------------------------%
%  Machine learning algorithms source codes demo version                  %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDT(trainlabel,trainfeat,testlabel,testfeat,nSplit,kfold)
rng('default'); 
% Perform decision tree
ModelDT=fitctree(trainfeat,trainlabel,'MaxNumSplits',nSplit);

PredTest = predict(ModelDT,testfeat);
PredTrain = predict(ModelDT,trainfeat);

% Confusion matrix
confmatTest=confusionmat(testlabel,PredTest);
confmatTrain=confusionmat(trainlabel,PredTrain);

% disp("*********DT*********")
% disp("Confusion Matrix Test set")
% disp(confmatTest)
%Calculate evaluation metrics on Test set
[AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest] = evalmetrics(confmatTest);

% disp("Confusion Matrix Train set")
% disp(confmatTrain)
%Calculate evaluation metrics on Train set
[AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = evalmetrics(confmatTrain);

%plot confusion matrix for both testing and training sets
% figure(11)
% nametest = 'DT-Testing-set-';
% plotconfusion(testlabel',PredTest',nametest);
% figure(12)
% nametrain = 'DT-Training-set-';
% plotconfusion(trainlabel',PredTrain',nametrain);
end

