%-----------------------------------GA Features------------------------
%---------------------------------Data with top 8 features from GA
clear; close all; clc;
%load global indexes
load trainingIndex7030.mat
load testingIndex7030.mat 
for j = 1:11
% disp('Binary GA Version**********');
%load dataset
disp(j)
load ./full_data/fmTop8PSO.mat;
featMatrixPSO = fmtop8PSO;
%For 70 - 30
% load trainidx7030.mat
% load testidx7030.mat
%For 75-25
%  load trainidx7525.mat
%  load testidx7525.mat
%For 80-20
% load trainidx8020.mat
% load testidx8020.mat

%Slicing the data
traindataPSO = featMatrixPSO(trainingIndex(:,j),:);
testdataPSO = featMatrixPSO(testingIndex(:,j),:);
%training Data
trainlabelPSO = traindataPSO(:,size(traindataPSO,2));
trainfeatPSO = traindataPSO(:,1:(size(traindataPSO,2)-1));
%test Data
testlabelPSO = testdataPSO(:,size(testdataPSO,2));
testfeatPSO = testdataPSO(:,1:(size(testdataPSO,2)-1));
%-----------------------------------Applying Classifiers with top 8 features-----------
% (1) Perform k-nearest neighbor (KNN)
    disp('KNN')
    kfold=10; k=3; % k-value in KNN
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jKNN(trainlabelPSO,trainfeatPSO,testlabelPSO,testfeatPSO,k,kfold); 
    %Save KNN evaluation metrics into vectors
    %testing metrics
    Accuracy_Test_KNNPSO(j) = AccuracyTest;
    Recall_Test_KNNPSO(j) = RecallTest;
    Specificity_Test_KNNPSO(j) = SpecificityTest;
    PPV_Test_KNNPSO(j) = PPVTest;
    NPV_Test_KNNPSO(j) = NPVTest;
    FMeasure_Test_KNNPSO(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_KNNPSO(j) = AccuracyTrain;
    Recall_Train_KNNPSO(j) = RecallTrain;
    Specificity_Train_KNNPSO(j) = SpecificityTrain;
    PPV_Train_KNNPSO(j) = PPVTrain;
    NPV_Train_KNNPSO(j) = NPVTrain;
    FMeasure_Train_KNNPSO(j) = FMeasureTrain;
    
    % (2) Perform discriminate analysis (DA)
%     disp("*********DA*********")
    kfold=10;
    % The Discriminate can selected as follows:
    % 'l' : linear 
    % 'q' : quadratic
    % 'pq': pseudoquadratic
    % 'pl': pseudolinear
    % 'dl': diaglinear
    % 'dq': diagquadratic
    Disc = {'l','q','pq','pl','dl','dq'};
    for i= 4:5
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDA(trainlabelPSO,trainfeatPSO,testlabelPSO,testfeatPSO,Disc{i},kfold,i);
        if(i==4)
            disp('DA-psuedolinear')
            %testing metrics
            Accuracy_Test_DAdlPSO(j) = AccuracyTest;
            Recall_Test_DAdlPSO(j) = RecallTest;
            Specificity_Test_DAdlPSO(j) = SpecificityTest;
            PPV_Test_DAdlPSO(j) = PPVTest;
            NPV_Test_DAdlPSO(j) = NPVTest;
            FMeasure_Test_DAdlPSO(j) = FMeasureTest;
            
            %training metrics
            Accuracy_Train_DAdlPSO(j) = AccuracyTrain;
            Recall_Train_DAdlPSO(j) = RecallTrain;
            Specificity_Train_DAdlPSO(j) = SpecificityTrain;
            PPV_Train_DAdlPSO(j) = PPVTrain;
            NPV_Train_DAdlPSO(j) = NPVTrain;
            FMeasure_Train_DAdlPSO(j) = FMeasureTrain;
            
        end
        if(i==5)
            disp('DA-diaglinear')
            %testing metrics
            Accuracy_Test_DAdqPSO(j) = AccuracyTest;
            Recall_Test_DAdqPSO(j) = RecallTest;
            Specificity_Test_DAdqPSO(j) = SpecificityTest;
            PPV_Test_DAdqPSO(j) = PPVTest;
            NPV_Test_DAdqPSO(j) = NPVTest;
            FMeasure_Test_DAdqPSO(j) = FMeasureTest;
            
            %training metrics
            Accuracy_Train_DAdqPSO(j) = AccuracyTrain;
            Recall_Train_DAdqPSO(j) = RecallTrain;
            Specificity_Train_DAdqPSO(j) = SpecificityTrain;
            PPV_Train_DAdqPSO(j) = PPVTrain;
            NPV_Train_DAdqPSO(j) = NPVTrain;
            FMeasure_Train_DAdqPSO(j) = FMeasureTrain;
        end
    end

    % (3) Perform Naive Bayes (NB)
%     disp("*********NB*********")
    kfold=10; Dist=['n','k']; 
    % The Distribution can selected as follows:
    % 'n' : normal distribution 
    % 'k' : kernel distribution
    for i= 1:2
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jNB(trainlabelPSO,trainfeatPSO,testlabelPSO,testfeatPSO,Dist(i),kfold,i); 
        if(i==1)
            disp('NB-normal')
            %testing metrics
            Accuracy_Test_NBnPSO(j) = AccuracyTest;
            Recall_Test_NBnPSO(j) = RecallTest;
            Specificity_Test_NBnPSO(j) = SpecificityTest;
            PPV_Test_NBnPSO(j) = PPVTest;
            NPV_Test_NBnPSO(j) = NPVTest;
            FMeasure_Test_NBnPSO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_NBnPSO(j) = AccuracyTrain;
            Recall_Train_NBnPSO(j) = RecallTrain;
            Specificity_Train_NBnPSO(j) = SpecificityTrain;
            PPV_Train_NBnPSO(j) = PPVTrain;
            NPV_Train_NBnPSO(j) = NPVTrain;
            FMeasure_Train_NBnPSO(j) = FMeasureTrain;
        end
        if(i==2)
            disp('NB-kernel')
            %testing metrics
            Accuracy_Test_NBkPSO(j) = AccuracyTest;
            Recall_Test_NBkPSO(j) = RecallTest;
            Specificity_Test_NBkPSO(j) = SpecificityTest;
            PPV_Test_NBkPSO(j) = PPVTest;
            NPV_Test_NBkPSO(j) = NPVTest;
            FMeasure_Test_NBkPSO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_NBkPSO(j) = AccuracyTrain;
            Recall_Train_NBkPSO(j) = RecallTrain;
            Specificity_Train_NBkPSO(j) = SpecificityTrain;
            PPV_Train_NBkPSO(j) = PPVTrain;
            NPV_Train_NBkPSO(j) = NPVTrain;
            FMeasure_Train_NBkPSO(j) = FMeasureTrain;
        end
    end

    % (4) Perform decision tree (DT)
    disp('DT')
    kfold=10; nSplit=50; % Number of split in DT
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jDT(trainlabelPSO,trainfeatPSO,testlabelPSO,testfeatPSO,nSplit,kfold);
    %testing metrics
    Accuracy_Test_DTPSO(j) = AccuracyTest;
    Recall_Test_DTPSO(j) = RecallTest;
    Specificity_Test_DTPSO(j) = SpecificityTest;
    PPV_Test_DTPSO(j) = PPVTest;
    NPV_Test_DTPSO(j) = NPVTest;
    FMeasure_Test_DTPSO(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_DTPSO(j) = AccuracyTrain;
    Recall_Train_DTPSO(j) = RecallTrain;
    Specificity_Train_DTPSO(j) = SpecificityTrain;
    PPV_Train_DTPSO(j) = PPVTrain;
    NPV_Train_DTPSO(j) = NPVTrain;
    FMeasure_Train_DTPSO(j) = FMeasureTrain;

    % (5) Perform support vector machine
    kernel = ['l','r','p'];
%     disp("*********SVM*********")
    for i= 1:3
        [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jSVM(trainlabelPSO,trainfeatPSO,testlabelPSO,testfeatPSO,kernel(i),i);
        if(i==1)
            disp('SVM-linear')
            %testing metrics
            Accuracy_Test_SVMlPSO(j) = AccuracyTest;
            Recall_Test_SVMlPSO(j) = RecallTest;
            Specificity_Test_SVMlPSO(j) = SpecificityTest;
            PPV_Test_SVMlPSO(j) = PPVTest;
            NPV_Test_SVMlPSO(j) = NPVTest;
            FMeasure_Test_SVMlPSO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMlPSO(j) = AccuracyTrain;
            Recall_Train_SVMlPSO(j) = RecallTrain;
            Specificity_Train_SVMlPSO(j) = SpecificityTrain;
            PPV_Train_SVMlPSO(j) = PPVTrain;
            NPV_Train_SVMlPSO(j) = NPVTrain;
            FMeasure_Train_SVMlPSO(j) = FMeasureTrain;
        end
        if(i==2)
            disp('SVM-Gaussian Radial Basis function')
            %testing metrics
            Accuracy_Test_SVMrPSO(j) = AccuracyTest;
            Recall_Test_SVMrPSO(j) = RecallTest;
            Specificity_Test_SVMrPSO(j) = SpecificityTest;
            PPV_Test_SVMrPSO(j) = PPVTest;
            NPV_Test_SVMrPSO(j) = NPVTest;
            FMeasure_Test_SVMrPSO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMrPSO(j) = AccuracyTrain;
            Recall_Train_SVMrPSO(j) = RecallTrain;
            Specificity_Train_SVMrPSO(j) = SpecificityTrain;
            PPV_Train_SVMrPSO(j) = PPVTrain;
            NPV_Train_SVMrPSO(j) = NPVTrain;
            FMeasure_Train_SVMrPSO(j) = FMeasureTrain;
        end
        if(i==3)
            disp('SVM-Polinomial-Degree2')
            %testing metrics
            Accuracy_Test_SVMpPSO(j) = AccuracyTest;
            Recall_Test_SVMpPSO(j) = RecallTest;
            Specificity_Test_SVMpPSO(j) = SpecificityTest;
            PPV_Test_SVMpPSO(j) = PPVTest;
            NPV_Test_SVMpPSO(j) = NPVTest;
            FMeasure_Test_SVMpPSO(j) = FMeasureTest;
            %training metrics
            Accuracy_Train_SVMpPSO(j) = AccuracyTrain;
            Recall_Train_SVMpPSO(j) = RecallTrain;
            Specificity_Train_SVMpPSO(j) = SpecificityTrain;
            PPV_Train_SVMpPSO(j) = PPVTrain;
            NPV_Train_SVMpPSO(j) = NPVTrain;
            FMeasure_Train_SVMpPSO(j) = FMeasureTrain;
        end
    end
    % 'r' : radial basis function  
    % 'l' : linear function 
    % 'p' : polynomial function 
    % 'g' : gaussian function

    % (6) Perform random forest (RF)
    disp('RF')
    kfold=10; nBag=50; % Number of bags in RF
    [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jRF(trainlabelPSO,trainfeatPSO,testlabelPSO,testfeatPSO,nBag,kfold);
    %testing metrics
    Accuracy_Test_RFPSO(j) = AccuracyTest;
    Recall_Test_RFPSO(j) = RecallTest;
    Specificity_Test_RFPSO(j) = SpecificityTest;
    PPV_Test_RFPSO(j) = PPVTest;
    NPV_Test_RFPSO(j) = NPVTest;
    FMeasure_Test_RFPSO(j) = FMeasureTest;
    %training metrics
    Accuracy_Train_RFPSO(j) = AccuracyTrain;
    Recall_Train_RFPSO(j) = RecallTrain;
    Specificity_Train_RFPSO(j) = SpecificityTrain;
    PPV_Train_RFPSO(j) = PPVTrain;
    NPV_Train_RFPSO(j) = NPVTrain;
    FMeasure_Train_RFPSO(j) = FMeasureTrain;
end
Accuracies_TestPSO = [Accuracy_Test_KNNPSO;Accuracy_Test_DAdlPSO;Accuracy_Test_DAdqPSO;Accuracy_Test_NBnPSO;Accuracy_Test_NBkPSO;Accuracy_Test_DTPSO;Accuracy_Test_SVMlPSO;Accuracy_Test_SVMrPSO;Accuracy_Test_SVMpPSO;Accuracy_Test_RFPSO];
Accuracies_TestPSO = Accuracies_TestPSO';
Accuracies_Test_meanPSO = mean(Accuracies_TestPSO);
Accuracies_TrainPSO = [Accuracy_Train_KNNPSO;Accuracy_Train_DAdlPSO;Accuracy_Train_DAdqPSO;Accuracy_Train_NBnPSO;Accuracy_Train_NBkPSO;Accuracy_Train_DTPSO;Accuracy_Train_SVMlPSO;Accuracy_Train_SVMrPSO;Accuracy_Train_SVMpPSO;Accuracy_Train_RFPSO];
Accuracies_TrainPSO = Accuracies_TrainPSO';
Accuracies_Train_meanPSO = mean(Accuracies_TrainPSO);
Recalls_TestPSO = [Recall_Test_KNNPSO;Recall_Test_DAdlPSO;Recall_Test_DAdqPSO;Recall_Test_NBnPSO;Recall_Test_NBkPSO; Recall_Test_DTPSO;Recall_Test_SVMlPSO;Recall_Test_SVMrPSO;Recall_Test_SVMpPSO;Recall_Test_RFPSO];
Recalls_TestPSO = Recalls_TestPSO';
Recalls_Test_meanPSO = mean(Recalls_TestPSO);
Recalls_TrainPSO = [Recall_Train_KNNPSO;Recall_Train_DAdlPSO;Recall_Train_DAdqPSO;Recall_Train_NBnPSO;Recall_Train_NBkPSO;Recall_Train_DTPSO; Recall_Train_SVMlPSO;Recall_Train_SVMrPSO;Recall_Train_SVMpPSO;Recall_Train_RFPSO];
Recalls_TrainPSO = Recalls_TrainPSO';
Recalls_Train_meanPSO = mean(Recalls_TrainPSO);
Specificities_TestPSO = [Specificity_Test_KNNPSO;Specificity_Test_DAdlPSO;Specificity_Test_DAdqPSO;Specificity_Test_NBnPSO;Specificity_Test_NBkPSO;Specificity_Test_DTPSO;Specificity_Test_SVMlPSO;Specificity_Test_SVMrPSO;Specificity_Test_SVMpPSO;Specificity_Test_RFPSO];
Specificities_TestPSO = Specificities_TestPSO';
Specificities_Test_meanPSO = mean(Specificities_TestPSO);
Specificities_TrainPSO = [Specificity_Train_KNNPSO;Specificity_Train_DAdlPSO;Specificity_Train_DAdqPSO;Specificity_Train_NBnPSO;Specificity_Train_NBkPSO;Specificity_Train_DTPSO;Specificity_Train_SVMlPSO;Specificity_Train_SVMrPSO;Specificity_Train_SVMpPSO;Specificity_Train_RFPSO];
Specificities_TrainPSO = Specificities_TrainPSO';
Specificities_Train_meanPSO = mean(Specificities_TrainPSO);
NTCPrecisions_TestPSO = [PPV_Test_KNNPSO;PPV_Test_DAdlPSO;PPV_Test_DAdqPSO;PPV_Test_NBnPSO;PPV_Test_NBkPSO;PPV_Test_DTPSO;PPV_Test_SVMlPSO;PPV_Test_SVMrPSO;PPV_Test_SVMpPSO;PPV_Test_RFPSO];
NTCPrecisions_TestPSO = NTCPrecisions_TestPSO';
NTCPrecisions_Test_meanPSO = mean(NTCPrecisions_TestPSO);
NTCPrecisions_TrainPSO = [PPV_Train_KNNPSO;PPV_Train_DAdlPSO;PPV_Train_DAdqPSO;PPV_Train_NBnPSO;PPV_Train_NBkPSO;PPV_Train_DTPSO;PPV_Train_SVMlPSO;PPV_Train_SVMrPSO;PPV_Train_SVMpPSO;PPV_Train_RFPSO];
NTCPrecisions_TrainPSO = NTCPrecisions_TrainPSO';
NTCPrecisions_Train_meanPSO = mean(NTCPrecisions_TrainPSO);
TCPrecisions_TestPSO = [NPV_Test_KNNPSO;NPV_Test_DAdlPSO;NPV_Test_DAdqPSO;NPV_Test_NBnPSO;NPV_Test_NBkPSO;NPV_Test_DTPSO;NPV_Test_SVMlPSO;NPV_Test_SVMrPSO;NPV_Test_SVMpPSO;NPV_Test_RFPSO];
TCPrecisions_TestPSO = TCPrecisions_TestPSO';
TCPrecisions_Test_meanPSO = mean(TCPrecisions_TestPSO);
TCPrecisions_TrainPSO = [NPV_Train_KNNPSO;NPV_Train_DAdlPSO;NPV_Train_DAdqPSO;NPV_Train_NBnPSO;NPV_Train_NBkPSO;NPV_Train_DTPSO;NPV_Train_SVMlPSO;NPV_Train_SVMrPSO;NPV_Train_SVMpPSO;NPV_Train_RFPSO];
TCPrecisions_TrainPSO = TCPrecisions_TrainPSO';
TCPrecisions_Train_meanPSO = mean(TCPrecisions_TrainPSO);
FMeasures_TestPSO = [FMeasure_Test_KNNPSO;FMeasure_Test_DAdlPSO;FMeasure_Test_DAdqPSO;FMeasure_Test_NBnPSO;FMeasure_Test_NBkPSO;FMeasure_Test_DTPSO;FMeasure_Test_SVMlPSO;FMeasure_Test_SVMrPSO;FMeasure_Test_SVMpPSO;FMeasure_Test_RFPSO];
FMeasures_TestPSO = FMeasures_TestPSO';
FMeasures_Test_meanPSO = mean(FMeasures_TestPSO);
FMeasures_TrainPSO = [FMeasure_Train_KNNPSO;FMeasure_Train_DAdlPSO;FMeasure_Train_DAdqPSO;FMeasure_Train_NBnPSO;FMeasure_Train_NBkPSO;FMeasure_Train_DTPSO;FMeasure_Train_SVMlPSO;FMeasure_Train_SVMrPSO;FMeasure_Train_SVMpPSO;FMeasure_Train_RFPSO];
FMeasures_TrainPSO = FMeasures_TrainPSO';
FMeasures_Train_meanPSO = mean(FMeasures_TrainPSO);
MeanMetrics_TestPSO = [Accuracies_Test_meanPSO;Recalls_Test_meanPSO;Specificities_Test_meanPSO;NTCPrecisions_Test_meanPSO;TCPrecisions_Test_meanPSO;FMeasures_Test_meanPSO];
MeanMetrics_TrainPSO = [Accuracies_Train_meanPSO;Recalls_Train_meanPSO;Specificities_Train_meanPSO;NTCPrecisions_Train_meanPSO;TCPrecisions_Train_meanPSO;FMeasures_Train_meanPSO];
%save BGA results
csvwrite('Accuracies_Test11runsPSO.csv',Accuracies_TestPSO);
csvwrite('Accuracies_Train11runsPSO.csv',Accuracies_TrainPSO);
csvwrite('Recalls_Test11runsPSO.csv',Recalls_TestPSO);
csvwrite('Recalls_Train11runsPSO.csv',Recalls_TrainPSO);
csvwrite('Specificities_Test11runsPSO.csv',Specificities_TestPSO);
csvwrite('Specificities_Train11runsPSO.csv',Specificities_TrainPSO);
csvwrite('NTCPrecisions_Test11runsPSO.csv',NTCPrecisions_TestPSO);
csvwrite('NTCPrecisions_Train11runsPSO.csv',NTCPrecisions_TrainPSO);
csvwrite('TCPrecisions_Test11runsPSO.csv',TCPrecisions_TestPSO);
csvwrite('TCPrecisions_Train11runsPSO.csv',TCPrecisions_TrainPSO);
csvwrite('FMeasures_Test11runsPSO.csv',FMeasures_TestPSO);
csvwrite('FMeasures_Train11runsPSO.csv',FMeasures_TrainPSO);
csvwrite('MeanMetrics_Test11runsPSO.csv',MeanMetrics_TestPSO);
csvwrite('MeanMetrics_Train11runsPSO.csv',MeanMetrics_TrainPSO);

%plot figures for Test data set
classifierNames = categorical({'KNN','DA-pl','DA-dl','NB-n','NB-k','DT','SVM-l','SVM-r','SVM-p2','RF'});
%Accuracies barh graphs
figure(1)
barh(classifierNames,Accuracies_Test_meanPSO)
title('Accuracy using BPSO Top-8 features');
xlabel('Accuracy');

%Recalls barh graphs
figure(2)
barh(classifierNames,Recalls_Test_meanPSO)
title('Recall using BPSO Top-8 features');
xlabel('Recall');

%Specificities barh graphs
figure(3)
barh(classifierNames,Specificities_Test_meanPSO)
title('Specificity using BPSO Top-8 features');
xlabel('Specificity');

%NTCPrecisions barh graphs
figure(4)
barh(classifierNames,NTCPrecisions_Test_meanPSO)
title('Non Tumor Class Precision using BPSO Top-8 features');
xlabel('Non Tumor Class Precision');

%TCPrecisions barh graphs
figure(5)
barh(classifierNames,TCPrecisions_Test_meanPSO)
title('Tumor Class Precision using BPSO Top-8 features');
xlabel('Tumor Class Precision');

%FMeasures barh graphs
figure(6)
barh(classifierNames,FMeasures_Test_meanPSO)
title('FMeasure using BPSO Top-8 features');
xlabel('FMeasure');