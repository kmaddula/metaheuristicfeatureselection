%-------------------------------------------------------------------------%
%  Machine learning algorithms source codes demo version                  %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function jKNN(trainlabel,trainfeat,testlabel,testfeat,k,kfold)
rng('default'); 
% Perform k-nearest neighbor classifier with Euclidean distance
ModelKNN=fitcknn(trainfeat,trainlabel,'NumNeighbors',k,'Distance','euclidean');
% Perform cross-validation
% C=crossval(Model,'KFold',kfold);
% Prediction
% Pred=kfoldPredict(C); 
PredTest = predict(ModelKNN,testfeat);
PredTrain = predict(ModelKNN,trainfeat);
% Confusion matrix
confmatTest = confusionmat(testlabel,PredTest); 
confmatTrain = confusionmat(trainlabel,PredTrain);
disp("*********KNN*********")

disp("Confusion Matrix Test set")
disp(confmatTest)
%Calculate evaluation metrics on Test set
evalmetrics(confmatTest);

disp("Confusion Matrix Train set")
disp(confmatTrain)
%Calculate evaluation metrics on Train set
evalmetrics(confmatTrain);

%plot confusion matrix for both testing and training sets
figure(1)
nametest = 'KNN-Testing-set-';
plotconfusion(testlabel',PredTest',nametest);
figure(2)
nametrain = 'KNN-Train-set-';
plotconfusion(trainlabel',PredTrain',nametrain);
end


