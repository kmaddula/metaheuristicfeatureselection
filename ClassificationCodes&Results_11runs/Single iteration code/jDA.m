%-------------------------------------------------------------------------%
%  Machine learning algorithms source codes demo version                  %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function jDA(trainlabel,trainfeat,testlabel,testfeat,Disc,kfold,i)
switch Disc
  case'l' ; Disc='linear'; 
  case'pq'; Disc='pseudoquadratic';
  case'q' ; Disc='quadratic';    
  case'dl'; Disc='diaglinear';
  case'pl'; Disc='pseudolinear'; 
  case'dq'; Disc='diagquadratic';
end
rng('default'); 
fprintf('------Discriminant: %s \n',Disc)
% Perform discriminate analysis classifier
ModelDA=fitcdiscr(trainfeat,trainlabel,'DiscrimType',Disc);

predTest = predict(ModelDA,testfeat);
predTrain = predict(ModelDA,trainfeat);

% Confusion matrix
confmatTest=confusionmat(testlabel,predTest); 
confmatTrain=confusionmat(trainlabel,predTrain);

disp("Confusion Matrix Test set")
disp(confmatTest)
%Calculate evaluation metrics on Test set
evalmetrics(confmatTest);

disp("Confusion Matrix Train set")
disp(confmatTrain)
%Calculate evaluation metrics on Test set
evalmetrics(confmatTrain);

%plot confusion matrix for both testing and training sets
figure(i*100)
nametest = ['DA-Testing-set-',Disc,'-'];
plotconfusion(testlabel',predTest',nametest);
figure(i*11)
nametrain = ['DA-Train-set-',Disc,'-'];
plotconfusion(trainlabel',predTrain',nametrain);
end



