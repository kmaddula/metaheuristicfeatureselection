%-----------------------------------PSO Features------------------------
%Data with top 8 features from PSO
clc, clear
%load dataset
load ./full_data/fmTop8PSO.mat;
featMatrix = fmtop8PSO;
%For 70 - 30
% load trainidx7030.mat
% load testidx7030.mat
%For 75-25
% load trainidx7525.mat
% load testidx7525.mat
%For 80-20
load trainidx8020.mat
load testidx8020.mat

%Slicing the data
traindata = featMatrix(trainidx,:);
testdata = featMatrix(testidx,:);
%training Data
trainlabel = traindata(:,size(traindata,2));
trainfeat = traindata(:,1:(size(traindata,2)-1));
%test Data
testlabel = testdata(:,size(testdata,2));
testfeat = testdata(:,1:(size(testdata,2)-1));
%-----------------------------------Applying Classifiers with top 8 features-----------
% (1) Perform k-nearest neighbor (KNN)
 kfold=10; k=3; % k-value in KNN
 jKNN(trainlabel,trainfeat,testlabel,testfeat,k,kfold); 

% (2) Perform discriminate analysis (DA)
 disp("*********DA*********")
 kfold=10;
% The Discriminate can selected as follows:
% 'l' : linear 
% 'q' : quadratic
% 'pq': pseudoquadratic
% 'pl': pseudolinear
% 'dl': diaglinear
% 'dq': diagquadratic
 Disc = {'l','q','pq','pl','dl','dq'};
 for i= 4:5
      jDA(trainlabel,trainfeat,testlabel,testfeat,Disc{i},kfold,i);
 end

% (3) Perform Naive Bayes (NB)
 disp("*********NB*********")
 kfold=10; Dist=['n','k']; 
% The Distribution can selected as follows:
% 'n' : normal distribution 
% 'k' : kernel distribution
 for i= 1:2
       jNB(trainlabel,trainfeat,testlabel,testfeat,Dist(i),kfold,i); 
 end

% (4) Perform decision tree (DT)
 kfold=10; nSplit=50; % Number of split in DT
 jDT(trainlabel,trainfeat,testlabel,testfeat,nSplit,kfold);

% (5) Perform support vector machine
 kernel = ['l','r','p'];
 disp("*********SVM*********")
for i= 1:3
     jSVM(trainlabel,trainfeat,testlabel,testfeat,kernel(i),i);
 end
% 'r' : radial basis function  
% 'l' : linear function 
% 'p' : polynomial function 
% 'g' : gaussian function

% (6) Perform random forest (RF)
 kfold=10; nBag=50; % Number of bags in RF
 jRF(trainlabel,trainfeat,testlabel,testfeat,nBag,kfold);


%Data with top 5 features from PSO

%Data with features having best Accuracy out of 11 runs of PSO