%-------------------------------------------------------------------------%
%  Machine learning algorithms source codes demo version                  %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function [AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest, AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = jRF(trainlabel,trainfeat,testlabel,testfeat,nBag,kfold)
rng('default');

pred2Test=[]; ytest2=[]; Afold=zeros(kfold,1); pred2Train=[]; ytrain2=[]; 
% Random forest start

  % Training the model
  ModelRF=TreeBagger(nBag,trainfeat,trainlabel,'OOBPred','On',...
    'Method','Classification');
  % Perform testing
  Pred0Test=predict(ModelRF,testfeat); Atest=size(Pred0Test,1); predTest=zeros(Atest,1); 
  Pred0Train=predict(ModelRF,trainfeat); Atrain=size(Pred0Train,1); predTrain=zeros(Atrain,1);
  % Convert format
  for j=1:Atest
  	predTest(j,1)=str2double(Pred0Test{j,1});
  end
  for j=1:Atrain
  	predTrain(j,1)=str2double(Pred0Train{j,1});
  end
 
  % Confusion matrix
  con=confusionmat(testlabel,predTest);
  % Accuracy for each fold
%   Afold(i)=100*sum(diag(con))/sum(con(:));  
  % Store temporary
  pred2Test=[pred2Test(1:end);predTest]; ytest2=[ytest2(1:end);testlabel];
  pred2Train=[pred2Train(1:end);predTrain]; ytrain2=[ytrain2(1:end);trainlabel];
% end
% Overall confusion matrix
confmatTest=confusionmat(ytest2,pred2Test);
confmatTrain=confusionmat(ytrain2,pred2Train);

% disp("*********Random Forest*********")

% disp("Confusion Matrix Test set")
% disp(confmatTest)
%Calculate Evaluation Metrics
[AccuracyTest, RecallTest, SpecificityTest, PPVTest, NPVTest, FMeasureTest] = evalmetrics(confmatTest);

% disp("Confusion Matrix Train set")
% disp(confmatTrain)
%Calculate Evaluation Metrics
[AccuracyTrain, RecallTrain, SpecificityTrain, PPVTrain, NPVTrain, FMeasureTrain] = evalmetrics(confmatTrain);

%plot confusion matrix for both testing and training sets
% figure(18)
% name = 'RF-Testing-set-';
% plotconfusion(ytest2',pred2Test',name);
% 
% figure(19)
% name = 'RF-Training-set-';
% plotconfusion(ytrain2',pred2Train',name);
end