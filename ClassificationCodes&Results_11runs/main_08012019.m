clc, clear
%load dataset
load ./full_data/featMatrix.mat;

% Diving data into testing & training sets
c = cvpartition(lmat,'HoldOut',0.20); % 30% testing and 70% training
trainidx = training(c); % generates logical vector of training index
testidx = test(c); % generates logical vector of testing index

% save partition indexes
save trainidx8020.mat trainidx
save testidx8020.mat testidx

%Original data with all features
traindata = featMatrix(trainidx,:);
testdata = featMatrix(testidx,:);
%training Data
trainlabel = traindata(:,14);
trainfeat = traindata(:,1:13);
%test Data
testlabel = testdata(:,14);
testfeat = testdata(:,1:13);
%-----------------------------------Applying Classifiers with all features-----------
% (1) Perform k-nearest neighbor (KNN)
 kfold=10; k=3; % k-value in KNN
 jKNN(trainlabel,trainfeat,testlabel,testfeat,k,kfold); 

% (2) Perform discriminate analysis (DA)
 disp("*********DA*********")
 kfold=10;
% The Discriminate can selected as follows:
% 'l' : linear 
% 'q' : quadratic
% 'pq': pseudoquadratic
% 'pl': pseudolinear
% 'dl': diaglinear
% 'dq': diagquadratic
 Disc = {'l','q','pq','pl','dl','dq'};
 for i= 4:5
      jDA(trainlabel,trainfeat,testlabel,testfeat,Disc{i},kfold,i);
 end

% (3) Perform Naive Bayes (NB)
 disp("*********NB*********")
 kfold=10; Dist=['n','k']; 
% The Distribution can selected as follows:
% 'n' : normal distribution 
% 'k' : kernel distribution
 for i= 1:2
       jNB(trainlabel,trainfeat,testlabel,testfeat,Dist(i),kfold,i); 
 end

% (4) Perform decision tree (DT)
 kfold=10; nSplit=50; % Number of split in DT
 jDT(trainlabel,trainfeat,testlabel,testfeat,nSplit,kfold);

% (5) Perform support vector machine
 kernel = ['l','r','p'];
 disp("*********SVM*********")
for i= 1:3
     jSVM(trainlabel,trainfeat,testlabel,testfeat,kernel(i),i);
 end
% 'r' : radial basis function  
% 'l' : linear function 
% 'p' : polynomial function 
% 'g' : gaussian function

% (6) Perform random forest (RF)
 kfold=10; nBag=50; % Number of bags in RF
 jRF(trainlabel,trainfeat,testlabel,testfeat,nBag,kfold);