*********KNN*********
Confusion Matrix Test set
    17    12
    15    31

Accuracy
    64

Recall
   53.1250

Specificity
   72.0930

PPV
   58.6207

NPV
   67.3913

FMeasure
   55.7377

Confusion Matrix Train set
    58    11
    12    97

Accuracy
   87.0787

Recall
   82.8571

Specificity
   89.8148

PPV
   84.0580

NPV
   88.9908

FMeasure
   83.4532

*********DA*********
------Discriminant: pseudolinear 
Confusion Matrix Test set
    14    15
     8    38

Accuracy
   69.3333

Recall
   63.6364

Specificity
   71.6981

PPV
   48.2759

NPV
   82.6087

FMeasure
   54.9020

Confusion Matrix Train set
    38    31
    18    91

Accuracy
   72.4719

Recall
   67.8571

Specificity
   74.5902

PPV
   55.0725

NPV
   83.4862

FMeasure
   60.8000

------Discriminant: diaglinear 
Confusion Matrix Test set
    12    17
    11    35

Accuracy
   62.6667

Recall
   52.1739

Specificity
   67.3077

PPV
   41.3793

NPV
   76.0870

FMeasure
   46.1538

Confusion Matrix Train set
    30    39
    22    87

Accuracy
   65.7303

Recall
   57.6923

Specificity
   69.0476

PPV
   43.4783

NPV
   79.8165

FMeasure
   49.5868

*********NB*********
------Distribution: normal 
Confusion Matrix Test set
    11    18
     7    39

Accuracy
   66.6667

Recall
   61.1111

Specificity
   68.4211

PPV
   37.9310

NPV
   84.7826

FMeasure
   46.8085

Confusion Matrix Train set
    25    44
    14    95

Accuracy
   67.4157

Recall
   64.1026

Specificity
   68.3453

PPV
   36.2319

NPV
   87.1560

FMeasure
   46.2963

------Distribution: kernel 
Confusion Matrix Test set
    12    17
    10    36

Accuracy
    64

Recall
   54.5455

Specificity
   67.9245

PPV
   41.3793

NPV
   78.2609

FMeasure
   47.0588

Confusion Matrix Train set
    30    39
    19    90

Accuracy
   67.4157

Recall
   61.2245

Specificity
   69.7674

PPV
   43.4783

NPV
   82.5688

FMeasure
   50.8475

*********DT*********
Confusion Matrix Test set
    16    13
    11    35

Accuracy
    68

Recall
   59.2593

Specificity
   72.9167

PPV
   55.1724

NPV
   76.0870

FMeasure
   57.1429

Confusion Matrix Train set
    61     8
     7   102

Accuracy
   91.5730

Recall
   89.7059

Specificity
   92.7273

PPV
   88.4058

NPV
   93.5780

FMeasure
   89.0511

*********SVM*********
------Kernel: linear 
Confusion Matrix Training set
    28    41
    13    96

Accuracy
   69.6629

Recall
   68.2927

Specificity
   70.0730

PPV
   40.5797

NPV
   88.0734

FMeasure
   50.9091

Confusion Matrix Testing set
    11    18
     7    39

Accuracy
   66.6667

Recall
   61.1111

Specificity
   68.4211

PPV
   37.9310

NPV
   84.7826

FMeasure
   46.8085

------Kernel: rbf 
Confusion Matrix Training set
    51    18
     1   108

Accuracy
   89.3258

Recall
   98.0769

Specificity
   85.7143

PPV
   73.9130

NPV
   99.0826

FMeasure
   84.2975

Confusion Matrix Testing set
    12    17
     8    38

Accuracy
   66.6667

Recall
    60

Specificity
   69.0909

PPV
   41.3793

NPV
   82.6087

FMeasure
   48.9796

------Kernel: polynomial 
Confusion Matrix Training set
    37    32
    19    90

Accuracy
   71.3483

Recall
   66.0714

Specificity
   73.7705

PPV
   53.6232

NPV
   82.5688

FMeasure
   59.2000

Confusion Matrix Testing set
    10    19
     4    42

Accuracy
   69.3333

Recall
   71.4286

Specificity
   68.8525

PPV
   34.4828

NPV
   91.3043

FMeasure
   46.5116

*********Random Forest*********
Confusion Matrix Test set
    16    13
     7    39

Accuracy
   73.3333

Recall
   69.5652

Specificity
    75

PPV
   55.1724

NPV
   84.7826

FMeasure
   61.5385

Confusion Matrix Train set
    69     0
     0   109

Accuracy
   100

Recall
   100

Specificity
   100

PPV
   100

NPV
   100

FMeasure
   100

>> 