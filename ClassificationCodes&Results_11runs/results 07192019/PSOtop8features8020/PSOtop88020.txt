*********KNN*********
Confusion Matrix Test set
     5    14
     6    25

Accuracy
    60

Recall
   45.4545

Specificity
   64.1026

PPV
   26.3158

NPV
   80.6452

FMeasure
   33.3333

Confusion Matrix Train set
    58    21
    12   112

Accuracy
   83.7438

Recall
   82.8571

Specificity
   84.2105

PPV
   73.4177

NPV
   90.3226

FMeasure
   77.8523

*********DA*********
------Discriminant: pseudolinear 
Confusion Matrix Test set
     9    10
     1    30

Accuracy
    78

Recall
    90

Specificity
    75

PPV
   47.3684

NPV
   96.7742

FMeasure
   62.0690

Confusion Matrix Train set
    44    35
    17   107

Accuracy
   74.3842

Recall
   72.1311

Specificity
   75.3521

PPV
   55.6962

NPV
   86.2903

FMeasure
   62.8571

------Discriminant: diaglinear 
Confusion Matrix Test set
     5    14
     4    27

Accuracy
    64

Recall
   55.5556

Specificity
   65.8537

PPV
   26.3158

NPV
   87.0968

FMeasure
   35.7143

Confusion Matrix Train set
    34    45
    25    99

Accuracy
   65.5172

Recall
   57.6271

Specificity
   68.7500

PPV
   43.0380

NPV
   79.8387

FMeasure
   49.2754

*********NB*********
------Distribution: normal 
Confusion Matrix Test set
     4    15
     2    29

Accuracy
    66

Recall
   66.6667

Specificity
   65.9091

PPV
   21.0526

NPV
   93.5484

FMeasure
   32.0000

Confusion Matrix Train set
    32    47
    16   108

Accuracy
   68.9655

Recall
   66.6667

Specificity
   69.6774

PPV
   40.5063

NPV
   87.0968

FMeasure
   50.3937

------Distribution: kernel 
Confusion Matrix Test set
     5    14
     4    27

Accuracy
    64

Recall
   55.5556

Specificity
   65.8537

PPV
   26.3158

NPV
   87.0968

FMeasure
   35.7143

Confusion Matrix Train set
    39    40
    15   109

Accuracy
   72.9064

Recall
   72.2222

Specificity
   73.1544

PPV
   49.3671

NPV
   87.9032

FMeasure
   58.6466

*********DT*********
Confusion Matrix Test set
    14     5
     9    22

Accuracy
    72

Recall
   60.8696

Specificity
   81.4815

PPV
   73.6842

NPV
   70.9677

FMeasure
   66.6667

Confusion Matrix Train set
    75     4
     7   117

Accuracy
   94.5813

Recall
   91.4634

Specificity
   96.6942

PPV
   94.9367

NPV
   94.3548

FMeasure
   93.1677

*********SVM*********
------Kernel: linear 
Confusion Matrix Training set
    13    66
     0   124

Accuracy
   67.4877

Recall
   100

Specificity
   65.2632

PPV
   16.4557

NPV
   100

FMeasure
   28.2609

Confusion Matrix Testing set
     2    17
     0    31

Accuracy
    66

Recall
   100

Specificity
   64.5833

PPV
   10.5263

NPV
   100

FMeasure
   19.0476

------Kernel: rbf 
Confusion Matrix Training set
    63    16
     3   121

Accuracy
   90.6404

Recall
   95.4545

Specificity
   88.3212

PPV
   79.7468

NPV
   97.5806

FMeasure
   86.8966

Confusion Matrix Testing set
     6    13
     5    26

Accuracy
    64

Recall
   54.5455

Specificity
   66.6667

PPV
   31.5789

NPV
   83.8710

FMeasure
    40

------Kernel: polynomial 
Confusion Matrix Training set
    25    54
    13   111

Accuracy
   66.9951

Recall
   65.7895

Specificity
   67.2727

PPV
   31.6456

NPV
   89.5161

FMeasure
   42.7350

Confusion Matrix Testing set
     7    12
     3    28

Accuracy
    70

Recall
    70

Specificity
    70

PPV
   36.8421

NPV
   90.3226

FMeasure
   48.2759

*********Random Forest*********
Confusion Matrix Test set
    11     8
     5    26

Accuracy
    74

Recall
   68.7500

Specificity
   76.4706

PPV
   57.8947

NPV
   83.8710

FMeasure
   62.8571

Confusion Matrix Train set
    79     0
     0   124

Accuracy
   100

Recall
   100

Specificity
   100

PPV
   100

NPV
   100

FMeasure
   100

>> 