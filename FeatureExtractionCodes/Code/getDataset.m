function [imds, imdsTrain,imdsValidation] = getDataset(dataPath)

    imds = imageDatastore(dataPath, 'IncludeSubfolders', true, 'LabelSource','foldernames');
    [imdsTrain,imdsValidation] = splitEachLabel(imds,0.7,'randomized');
    
end