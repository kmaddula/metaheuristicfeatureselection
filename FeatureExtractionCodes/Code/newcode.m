%------------------Reading the dataset-------------------------------------
dataPath = '../dataset';
[imds,imdsTrain,imdsValidation] = getDataset(dataPath);
labels = imds.Labels;
labels = labels == 'yes';
%--------------Matrix to store feature values & corresponding labels-------
featMatrix = [];
%----------------Repeate the loop through all 253 images-------------------
for c = 1:1
    c %output the number of iteration
    %---------------------Resizing images----------------------------------
    %imds= imresize(P,[200,200]);
    %for debugging: Selecting 114 image with tumor
    for i = 1:114
        img = read(imds);
    end
%     img = read(imds);
    img = imresize(img,[200,200]);
    [x,y,z] = size(img);
    %----------------Convert to grayscale only if RGB image----------------
    if z == 3
        img = rgb2gray(img);
    end
    %************************************segmentation is done here*************
    % Otsu Binarization for segmentation
    level = graythresh(img);
    %gray = gray>80;
    %img = im2bw(img,.6); %------Kuladeep: commented, as im2bw() is almost outdated
	img = imbinarize(img,.6);
    %img = bwareaopen(img,80); %------Kuladeep: commented, to rename img to img2
	img2 = bwareaopen(img,80);
    %img2 = im2bw(img); %------Kuladeep: commented, as image is already binary
    % Try morphological operations
    %gray = rgb2gray(I);
    %tumor = imopen(gray,strel('line',15,0));
    
    %**************************feature extraction using DWT****************
    signal1 = img2(:,:);
    [cA1,cH1,cV1,cD1] = dwt2(signal1,'db4');
    [cA2,cH2,cV2,cD2] = dwt2(cA1,'db4');
    [cA3,cH3,cV3,cD3] = dwt2(cA2,'db4');

    DWT_feat = [cA3,cH3,cV3,cD3];
    %---------------******PCA to decrease number of features******---------
    G = pca(DWT_feat);
    whos DWT_feat
    save DWT_feat
    whos G
    save G
    g = graycomatrix(G);
   
    stats = graycoprops(g,'Contrast Correlation Energy Homogeneity');
    %---------Deriving statistical parameters as features------------------
    Contrast = stats.Contrast;
    Correlation = stats.Correlation;
    Energy = stats.Energy;
    Homogeneity = stats.Homogeneity;
    Mean = mean2(G);
    Standard_Deviation = std2(G);
    Entropy = entropy(G);
    RMS = mean2(rms(G));
    %Skewness = skewness(img)
    Variance = mean2(var(double(G)));
    a = sum(double(G(:)));
    Smoothness = 1-(1/(1+a));
    Kurtosis = kurtosis(double(G(:)));
    Skewness = skewness(double(G(:)));
    % Inverse Difference Movement
    m = size(G,1);
    n = size(G,2);
    in_diff = 0;
    for i = 1:m
        for j = 1:n
          temp = G(i,j)./(1+(i-j).^2);
          in_diff = in_diff+temp;
        end
    end
    IDM = double(in_diff);
    
    %-------------feature vector for one image-----------------------------
    feat = [Contrast,Correlation,Energy,Homogeneity, Mean, Standard_Deviation, Entropy, RMS, Variance, Smoothness, Kurtosis, Skewness, IDM];
   
    %---------input features and its label value into a matrix----------------
    featMatrix = [featMatrix;feat,labels(c)];
end


%------------------------write featMatrix with labels into a csv file----------
csvwrite('featMatrix.csv',featMatrix);

%--------------- Feature Selection -------------------------------







%--------------- Applying Classifiers ----------------------------







