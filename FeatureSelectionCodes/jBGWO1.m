%-------------------------------------------------------------------------%
%  Binary Grey Wolf Optimization (BGWO) source codes demo version         %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function [sFeat,Sf,Nf,curve,AverageFitnessGWO]=jBGWO1(feat,label,N,T,s)%Kuladeep: added "AverageFitnessGWO" as return value
%---Inputs-----------------------------------------------------------------
% feat:  features
% label: labelling
% N:     Number of wolves
% T:     Maximum number of iterations
%---Outputs----------------------------------------------------------------
% sFeat: Selected features
% Sf:    Selected feature index
% Nf:    Number of selected features
% curve: Convergence curve
%--------------------------------------------------------------------------

% Objective function
fun=@jFitnessFunction; 
% Number of dimensions
D=size(feat,2); 
% Initial Population
X=zeros(N,D); fit=zeros(1,N);
for i=1:N
  for d=1:D
    if rand() > 0.5
      X(i,d)=1;
    end
  end
end
% Fitness 
for i=1:N
  fit(i)=fun(feat,label,X(i,:));
end
% Sort fitness  
[~,idx]=sort(fit);  
% Update alpha, beta & delta wolves
Xalpha=X(idx(1),:); Xbeta=X(idx(2),:); Xdelta=X(idx(3),:);
Falpha=fit(idx(1)); Fbeta=fit(idx(2)); Fdelta=fit(idx(3));
% Pre
curve=inf; t=1; 
s = s*100;
figure(s); clf; axis([1 100 0 0.5]); xlabel('Number of Iterations');
ylabel('Fitness Value'); title('Convergence Curve'); grid on;
%---Iterations start-------------------------------------------------------
AverageFitnessGWO = 0; %Kuladeep: to calculate average fitness for one run
while t <= T
	% Coefficient decreases linearly from 2 to 0 Eq(17)
  a=2-2*(t/T); 
  for i=1:N
    for d=1:D
      % Parameter C Eq(16)
      C1=2*rand(); C2=2*rand(); C3=2*rand();
      % Compute Dalpha, Dbeta & Ddelta Eq(22-24)
      Dalpha=abs(C1*Xalpha(d)-X(i,d)); Dbeta=abs(C2*Xbeta(d)-X(i,d));
      Ddelta=abs(C3*Xdelta(d)-X(i,d));
      % Parameter A Eq(15)
      A1=2*a*rand()-a; 
      % Compute Bstep Eq(27,30,33)
      Bstep1=jBstepBGWO(A1*Dalpha); Bstep2=jBstepBGWO(A1*Dbeta); 
      Bstep3=jBstepBGWO(A1*Ddelta);
      % Wolf update Eq(26,29,32)
      X1=jBGWOupdate(Xalpha(d),Bstep1); X2=jBGWOupdate(Xbeta(d),Bstep2);
      X3=jBGWOupdate(Xdelta(d),Bstep3);
      % Crossover update wolf Eq(25)
      X(i,d)=jCrossoverBGWO(X1,X2,X3);
    end
  end
  for i=1:N
    %----------------------------Start:checking for all 0's --------------------------------- 
    %to check if the particle has all 0's selected as features
	%new way to validate for all 0's ~updated on 10/1/2019
	while(~any(X(i,:)))
		fprintf('Iteration Number *%d*: Resetting Particle Postion to new position \n',t); %for debugging purpose
		X(i,:) = validateIndividual(X(i,:));
	end
    %----------------------------End:checking for all 0's -----------------------------------
    % Fitness 
    fit(i)=fun(feat,label,X(i,:));
    % Update alpha, beta & delta 
    if fit(i) < Falpha
      Falpha=fit(i); Xalpha=X(i,:);
    end
    if fit(i) < Fbeta && fit(i) > Falpha
      Fbeta=fit(i); Xbeta=X(i,:);
    end
    if fit(i) < Fdelta && fit(i) > Falpha && fit(i) > Fbeta
      Fdelta=fit(i); Xdelta=X(i,:);
    end
  end
  curve(t)=Falpha; 
  AverageFitnessGWO = AverageFitnessGWO + Falpha; %Kuladeep: finding total fitness for the 100 generations
  % Plot convergence curve
  pause(0.000000001); hold on;
  CG=plot(t,Falpha,'Color','r','Marker','.'); set(CG,'MarkerSize',5);
  t=t+1;
end
AverageFitnessGWO = AverageFitnessGWO/T; %Kuladeep: to find average fitness for 100 generations
c = ['redCurveBGWO' num2str(s/100)]; saveas(gcf,c,'jpeg');
% Select features based on selected index 
Pos=1:D; Sf=Pos(Xalpha==1); Nf=length(Sf); sFeat=feat(:,Sf); 
end

%--------------------------------function to validate for all 0's in individual ~updated on 10/1/2019
function Xupdate = validateIndividual(Xupdate)
   for d=1:13
    if rand() > 0.5
      Xupdate(1,d)=1;
    end
  end
end







