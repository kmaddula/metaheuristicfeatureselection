%-------------------------------------------------------------------------%
%  Binary Grey Wolf Optimization (BGWO) source codes demo version         %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function Y=jCrossoverBGWO(X1,X2,X3)
% Crossover Eq(35)
r=rand();
if r < 1/3
	Y=X1;
elseif r < 2/3 && r >=1/3
	Y=X2;
else
	Y=X3;
end
end

