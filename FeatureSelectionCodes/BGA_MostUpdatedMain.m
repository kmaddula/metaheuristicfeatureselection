clc, clear, close 
load fmat.mat; %--------edited by kuladeep
load lmat.mat; %--------edited by kuladeep
feat = fmat; label = lmat; %-------------edited by kuladeep
%Save selected feature indexes in each run into 'selFeatureIndexes' matrix
%Save Average fitness value for each run in 'AverageFitnessVector' vector
selFeatureIndexesGA = zeros(11,13); % For 11 runs of GA
AverageFitnessVectorGA = zeros(1,11);% For 11 runs of GA
%-----------------------------GA repeat for 11 times-----------------------------
totalCurveDataBGA = zeros(11,1000); %matrix to save curve data for all 11 runs.
tic
disp('Executing GA for 11 times')
for i = 1:11
    %N=10; T=100; CR=0.8; MR=0.01; %-----commented by Kuladeep
    N=100; T=1000; CR=0.8; MR=0.01; %---------eidted by Kuladeep: Increased population and iteration size
    disp('Iteration Number')
    disp(i)
    [sFeat,Sf,Nf,curve,AverageFitnessGA]=jGA(feat,label,N,T,CR,MR,i);
    AverageFitnessVectorGA(1,i) = AverageFitnessGA;
    disp('Size of Feature matrix after performing Feature selection is:')
    disp(size(sFeat,2))
    disp('Selected features after performing Feature selection are:')
    disp(Sf)
    for j = 1:Nf
        selFeatureIndexesGA(i,Sf(j)) = 1;
    end
    % Plot convergence curve
    figure(i); plot(1:T,curve); xlabel('Number of Iterations');
    ylabel('Fitness Value'); title('BGA'); grid on;
    c = ['BgaConCurve' num2str(i)]; saveas(gcf,c,'jpeg');
    totalCurveDataBGA(i,:) = curve; %curve data getting added each and every run
    
    
end
toc
ElapsedTimeGA = toc;
%Calculate feature count vector for the 11 runs of BGA
FeatCountBGA = sum(selFeatureIndexesGA);
%Calculate top 8,9,10,11 feature matrices from FeatCountBGA
%-----top8 feature matrix
top8indexesBGA = zeros(1,13);
[values,top8BGA] = maxk(FeatCountBGA,8);
for i = 1:8
    top8indexesBGA(top8BGA(i)) = 1;
 end
fmtop8BGA = [fmat(:,top8indexesBGA==1),lmat];
%-----top9 feature matrix
top9indexesBGA = zeros(1,13);
[values,top9BGA] = maxk(FeatCountBGA,9);
for i = 1:9
    top9indexesBGA(top9BGA(i)) = 1;
 end
fmtop9BGA = [fmat(:,top9indexesBGA==1),lmat];
%-----top10 feature matrix
top10indexesBGA = zeros(1,13);
[values,top10BGA] = maxk(FeatCountBGA,10);
for i = 1:10
    top10indexesBGA(top10BGA(i)) = 1;
 end
fmtop10BGA = [fmat(:,top10indexesBGA==1),lmat];
%-----top10 feature matrix
top11indexesBGA = zeros(1,13);
[values,top11BGA] = maxk(FeatCountBGA,11);
for i = 1:11
    top11indexesBGA(top11BGA(i)) = 1;
 end
fmtop11BGA = [fmat(:,top11indexesBGA==1),lmat];

%save all results for BGA
csvwrite('selFeatureIndexesGA.csv',selFeatureIndexesGA);
csvwrite('AverageFitnessVectorGA.csv',AverageFitnessVectorGA);

csvwrite('top8indexesBGA.csv',top8indexesBGA);
csvwrite('top9indexesBGA.csv',top9indexesBGA);
csvwrite('top10indexesBGA.csv',top10indexesBGA);
csvwrite('top11indexesBGA.csv',top11indexesBGA);

csvwrite('fmtop8BGA.csv',fmtop8BGA);
csvwrite('fmtop9BGA.csv',fmtop9BGA);
csvwrite('fmtop10BGA.csv',fmtop10BGA);
csvwrite('fmtop11BGA.csv',fmtop11BGA);