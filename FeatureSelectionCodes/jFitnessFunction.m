%-------------------------------------------------------------------------%
%  Fitness Function (Error Rate) source codes demo version                %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%  

function fitness=jFitnessFunction(feat,label,X)
% Parameter setting for number of cross-validation
kfold=10;
% Error rate
fitness=jwrapperSVM(feat(:,X==1),label,kfold);
end

function ER=jwrapperSVM(feat,label,kfold)
%Model=fitcknn(feat,label,'NumNeighbors',k,'Distance','euclidean'); 
Model = fitcsvm(feat,label,'KernelFunction','rbf','KernelScale','auto','Standardize',true,'ClassNames',[0,1]);
C=crossval(Model,'KFold',kfold);
% Error rate 
ER=kfoldLoss(C);
end






