%-------------------------------------------------------------------------%
%  Genetic Algorithm (GA) source codes demo version                       %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function [sFeat,Sf,Nf,curve,AverageFitnessGA]=jGA(feat,label,N,T,CR,MR,s)%Kuladeep: added "AverageFitnessGA" as return value
%---Inputs-----------------------------------------------------------------
% feat:  features
% label: labelling
% N:     Number of chromosomes
% T:     Maximum number of generations
% CR:    Crossover rate
% MR:    Mutation rate
%---Outputs----------------------------------------------------------------
% sFeat: Selected features
% Sf:    Selected feature index
% Nf:    Number of selected features
% curve: Convergence curve
%--------------------------------------------------------------------------

% Objective function
fun=@jFitnessFunction; 
% Number of dimensions
D=size(feat,2);
% Initial population
X=zeros(N,D); fit=zeros(1,N);
for i=1:N
  for d=1:D
    if rand() > 0.5
      X(i,d)=1;
    end
  end
end
% Fitness 
for i=1:N
  fit(i)=fun(feat,label,X(i,:));
end

curve=inf; t=1; 
%figure(1); clf; axis([1 100 0 0.5]); xlabel('Number of Iterations'); %--commented by Kuladeep
s = s*100;
figure(s); clf; axis([1 1000 0 0.5]); xlabel('Number of Iterations'); %--edited by Kuladeep: Removed default axis length
ylabel('Fitness Value'); title('Convergence Curve'); grid on;
%---Generations start------------------------------------------------------
AverageFitnessGA = 0; %Kuladeep: to calculate average fitness for one run
while t <= T
	% Convert error to accuracy (inverse of fitness)
  Ifit=1-fit;
  % Get probability
  Prob=Ifit/sum(Ifit);
  % {1} Crossover 
  X1=zeros(1,D); X2=zeros(1,D); z=1;
  for i=1:N
    if rand() < CR
      % Select two parents 
      k1=jRouletteWheelSelection(Prob); k2=jRouletteWheelSelection(Prob);
      % Store parents 
      P1=X(k1,:); P2=X(k2,:);
      % Random select one crossover point
      ind=randi([1,D]);
      % Single point crossover between 2 parents
      X1(z,:)=[P1(1:ind),P2(ind+1:D)]; 
      X2(z,:)=[P2(1:ind),P1(ind+1:D)]; z=z+1;
    end
  end
  % Union
  Xnew=[X1;X2]; Nc=size(Xnew,1); Fnew=zeros(1,Nc);
  % {2} Mutation
  for i=1:Nc
    for d=1:D
      if rand() <= MR
        % Mutate from '0' to '1' or '1' to '0'
        Xnew(i,d)=1-Xnew(i,d);
      end
    end
    %----------------------------Start:checking for all 0's --------------------------------- 
	%new way to validate for all 0's ~updated on 10/1/2019
	while(~any(Xnew(i,:)))
		fprintf('Iteration Number *%d*: Resetting Particle Postion to new position \n',t); %for debugging purpose
		Xnew(i,:) = validateIndividual(Xnew(i,:));
	end
    %----------------------------End:checking for all 0's -----------------------------------
    % Fitness 
    Fnew(i)=fun(feat,label,Xnew(i,:));
  end 
  % Merge population
  XX=[X;Xnew]; FF=[fit,Fnew]; 
  % Select nPop best solution 
  [FF,idx]=sort(FF); X=XX(idx(1:N),:); fit=FF(1:N); 
  % Best chromosome
  Xgb=X(1,:); fitG=fit(1); curve(t)=fitG;
  AverageFitnessGA = AverageFitnessGA + fitG; %Kuladeep: finding total fitness for the 1000 generations
  % Plot convergence curve
  pause(0.000000001); hold on;
  CG=plot(t,fitG,'Color','r','Marker','.'); set(CG,'MarkerSize',5);
  curve(t) = fitG;
  t=t+1;
end
% Save convergence curve
c = ['redCurveBGA' num2str(s/100)]; saveas(gcf,c,'jpeg');

AverageFitnessGA = AverageFitnessGA/T; %Kuladeep: to find average fitness for 1000 generations
% Select features based on selected index
Pos=1:D; Sf=Pos(Xgb==1); Nf=length(Sf); sFeat=feat(:,Sf); 
end

%--------------------------------function to validate for all 0's in individual ~updated on 10/1/2019
function Xupdate = validateIndividual(Xupdate)
   for d=1:13
    if rand() > 0.5
      Xupdate(1,d)=1;
    end
  end
end

