%-------------------------------------------------------------------------%
%  Binary Grey Wolf Optimization (BGWO) source codes demo version         %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function Bstep=jBstepBGWO(AD)
% Cstep Eq(28,31,34)
Cstep=1/(1+exp(-10*(AD-0.5))); 
% Bstep Eq(27,30,33)
if Cstep >= rand() 
	Bstep=1; 
else
	Bstep=0;
end
end

