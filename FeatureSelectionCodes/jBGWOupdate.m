%-------------------------------------------------------------------------%
%  Binary Grey Wolf Optimization (BGWO) source codes demo version         %
%                                                                         %
%  Programmer: Jingwei Too                                                %
%                                                                         %
%  E-Mail: jamesjames868@gmail.com                                        %
%-------------------------------------------------------------------------%

function Y=jBGWOupdate(X,Bstep) 
% Position update Eq(26,29,32)
if (X+Bstep) >= 1
	Y=1;
else
  Y=0;
end
end

