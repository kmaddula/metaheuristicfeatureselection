clc, clear, close 
load fmat.mat; %--------edited by kuladeep
load lmat.mat; %--------edited by kuladeep
feat = fmat; label = lmat; %-------------edited by kuladeep
%Save selected feature indexes in each run into 'selFeatureIndexes' matrix
%Save Average fitness value for each run in 'AverageFitnessVector' vector
selFeatureIndexesGWO = zeros(11,13);% For 11 runs of GWO
AverageFitnessVectorGWO = zeros(1,11);% For 11 runs of GWO

% %-----------------------------GWO repeat for 11 times-------------------------------
totalCurveDataBGWO = zeros(11,1000); %matrix to save curve data for all 11 runs.
tic
close all;
disp('Executing GWO for 11 times')
j=2; % for displaying plot from figure2 onwards until 32 to not interfer with figure from algorithm
for i = 1:11
	N=100; T=1000;
	[sFeat,Sf,Nf,curve,AverageFitnessGWO]=jBGWO1(feat,label,N,T,i); %Kuladeep: added AverageFitness as extra return value
	disp('Iteration Number')
    disp(i)
	AverageFitnessVectorGWO(1,i) = AverageFitnessGWO;
	disp('Size of Feature matrix after performing Feature selection is:')
    disp(size(sFeat,2))
    disp('Selected features after performing Feature selection are:')
    disp(Sf)
	% Plot convergence curve
	figure(j); plot(1:T,curve); xlabel('Number of Iterations');
	ylabel('Fitness Value'); title('BGWO'); grid on;
    c = ['GwoConCurve' num2str(j)]; saveas(gcf,c,'jpeg');
	j = j+1;
    for deepu = 1:Nf
        selFeatureIndexesGWO(i,Sf(deepu)) = 1;
    end
    totalCurveDataBGWO(i,:) = curve; %curve data getting added each and every run
end
ElapsedTimeGWO = toc;
save selFeatureIndexesGWO;
save AverageFitnessVectorGWO;

%Calculate feature count vector for the 11 runs of BGWO
FeatCountBGWO = sum(selFeatureIndexesGWO);
%Calculate top 8,9,10,11 feature matrices from FeatCountBGWO
%-----top8 feature matrix
top8indexesBGWO = zeros(1,13);
[values,top8BGWO] = maxk(FeatCountBGWO,8);
for i = 1:8
    top8indexesBGWO(top8BGWO(i)) = 1;
 end
fmtop8BGWO = [fmat(:,top8indexesBGWO==1),lmat];
%-----top9 feature matrix
top9indexesBGWO = zeros(1,13);
[values,top9BGWO] = maxk(FeatCountBGWO,9);
for i = 1:9
    top9indexesBGWO(top9BGWO(i)) = 1;
 end
fmtop9BGWO = [fmat(:,top9indexesBGWO==1),lmat];
%-----top10 feature matrix
top10indexesBGWO = zeros(1,13);
[values,top10BGWO] = maxk(FeatCountBGWO,10);
for i = 1:10
    top10indexesBGWO(top10BGWO(i)) = 1;
 end
fmtop10BGWO = [fmat(:,top10indexesBGWO==1),lmat];
%-----top10 feature matrix
top11indexesBGWO = zeros(1,13);
[values,top11BGWO] = maxk(FeatCountBGWO,11);
for i = 1:11
    top11indexesBGWO(top11BGWO(i)) = 1;
 end
fmtop11BGWO = [fmat(:,top11indexesBGWO==1),lmat];

%save all results for BGWO
csvwrite('selFeatureIndexesGWO.csv',selFeatureIndexesGWO);
csvwrite('AverageFitnessVectorGWO.csv',AverageFitnessVectorGWO);

csvwrite('top8indexesBGWO.csv',top8indexesBGWO);
csvwrite('top9indexesBGWO.csv',top9indexesBGWO);
csvwrite('top10indexesBGWO.csv',top10indexesBGWO);
csvwrite('top11indexesBGWO.csv',top11indexesBGWO);

csvwrite('fmtop8BGWO.csv',fmtop8BGWO);
csvwrite('fmtop9BGWO.csv',fmtop9BGWO);
csvwrite('fmtop10BGWO.csv',fmtop10BGWO);
csvwrite('fmtop11BGWO.csv',fmtop11BGWO);