clc, clear, close 
load fmat.mat; %--------edited by kuladeep
load lmat.mat; %--------edited by kuladeep
feat = fmat; label = lmat; %-------------edited by kuladeep
%Save selected feature indexes in each run into 'selFeatureIndexes' matrix
%Save Average fitness value for each run in 'AverageFitnessVector' vector
selFeatureIndexesPSO = zeros(11,13);% For 11 runs of PSO
AverageFitnessVectorPSO = zeros(1,11);% For 11 runs of PSO

%-----------------------------PSO repeat for 11 times-------------------------------
totalCurveDataBPSO = zeros(11,1000); %matrix to save curve data for all 11 runs.
tic
disp('Executing PSO for 11 times')
for i = 1:11
    %N=10; T=100; 
    N = 100; T = 1000; %Increased population and iteration size
    c1=2; c2=2; Vmax=6; Vmin=-6; Wmax=0.9; Wmin=0.4;
    disp('Iteration Number')
    disp(i)
    [sFeat,Sf,Nf,curve,AverageFitnessPSO]=jBPSO(feat,label,N,T,c1,c2,Wmax,Wmin,Vmax,Vmin,i);
    AverageFitnessVectorPSO(1,i) = AverageFitnessPSO;
    disp('Size of Feature matrix after performing Feature selection is:')
    disp(size(sFeat,2))
    disp('Selected features after performing Feature selection are:')
    disp(Sf)
    
    % Plot convergence curve
    figure(i); plot(1:T,curve); xlabel('Number of Iterations');
    ylabel('Fitness Value'); title('BPSO'); grid on;
    c = ['PsoConCurve' num2str(i)]; saveas(gcf,c,'jpeg');
    
    for j = 1:Nf
        selFeatureIndexesPSO(i,Sf(j)) = 1;
    end
    totalCurveDataBPSO(i,:) = curve; %curve data getting added each and every run
end
toc
ElapsedTimePSO = toc;
save selFeatureIndexesPSO;
save AverageFitnessVectorPSO;
%Calculate feature count vector for the 11 runs of BPSO
FeatCountBPSO = sum(selFeatureIndexesPSO);
%Calculate top 8,9,10,11 feature matrices from FeatCountBPSO
%-----top8 feature matrix
top8indexesBPSO = zeros(1,13);
[values,top8BPSO] = maxk(FeatCountBPSO,8);
for i = 1:8
    top8indexesBPSO(top8BPSO(i)) = 1;
 end
fmtop8BPSO = [fmat(:,top8indexesBPSO==1),lmat];
%-----top9 feature matrix
top9indexesBPSO = zeros(1,13);
[values,top9BPSO] = maxk(FeatCountBPSO,9);
for i = 1:9
    top9indexesBPSO(top9BPSO(i)) = 1;
 end
fmtop9BPSO = [fmat(:,top9indexesBPSO==1),lmat];
%-----top10 feature matrix
top10indexesBPSO = zeros(1,13);
[values,top10BPSO] = maxk(FeatCountBPSO,10);
for i = 1:10
    top10indexesBPSO(top10BPSO(i)) = 1;
 end
fmtop10BPSO = [fmat(:,top10indexesBPSO==1),lmat];
%-----top11 feature matrix
top11indexesBPSO = zeros(1,13);
[values,top11BPSO] = maxk(FeatCountBPSO,11);
for i = 1:11
    top11indexesBPSO(top11BPSO(i)) = 1;
 end
fmtop11BPSO = [fmat(:,top11indexesBPSO==1),lmat];

%save all results for BPSO
csvwrite('selFeatureIndexesPSO.csv',selFeatureIndexesPSO);
csvwrite('AverageFitnessVectorPSO.csv',AverageFitnessVectorPSO);

csvwrite('top8indexesBPSO.csv',top8indexesBPSO);
csvwrite('top9indexesBPSO.csv',top9indexesBPSO);
csvwrite('top10indexesBPSO.csv',top10indexesBPSO);
csvwrite('top11indexesBPSO.csv',top11indexesBPSO);

csvwrite('fmtop8BPSO.csv',fmtop8BPSO);
csvwrite('fmtop9BPSO.csv',fmtop9BPSO);
csvwrite('fmtop10BPSO.csv',fmtop10BPSO);
csvwrite('fmtop11BPSO.csv',fmtop11BPSO);